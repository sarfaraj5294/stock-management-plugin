$ = jQuery;
$(document).ready(function ()
{
    
    
    
    $('.hifi-filter-cat').live('change', function(){
    var catFilter = $(this).val();
    if( catFilter != '' && catFilter != 'all' ){
        document.location.href = 'admin.php?page=hifi_manage_stock'+catFilter;    
    }
    else if(catFilter == 'all' )
    {
        document.location.href = 'admin.php?page=hifi_manage_stock';
    }
});
    $('.hifi-filter-company').live('change', function(){
    var companyFilter = $(this).val();
    if( companyFilter != '' && companyFilter != 'all' ){
        document.location.href = 'admin.php?page=hifi_manage_stock'+companyFilter;
    }
    else if(companyFilter == 'all' )
    {
        document.location.href = 'admin.php?page=hifi_manage_stock';
    }
});
    
     jQuery.validator.addMethod("noSpace", function (value, element) {
        return value.trim() != "";
    }, admin_veriables.no_space);
    
    var company_form_rules={
        'company_title' :{
               'required' : true,
                noSpace: true
        },
        'company_desc' :{
               'required' : true,
                noSpace: true
        },
    };
    var company_form_msg={
        noSpace:admin_veriables.no_space
    }
     var company_form_args = {
         rules: company_form_rules,
         message:company_form_msg,
            onkeyup: false,
            errorClass: 'hifi-validation-error',
        
     }
    var category_form_rules={
        'category_title' :{
               'required' : true,
                noSpace: true
        },
        'category_desc' :{
               'required' : true,
                noSpace: true
        },
    };
    var category_form_msg={
        noSpace:admin_veriables.no_space
    }
     var category_form_args = {
         rules: category_form_rules,
         message:category_form_msg,
            onkeyup: false,
            errorClass: 'hifi-validation-error',
        
     }
    jQuery("#company_form").validate(company_form_args);
    jQuery("#category_form").validate(category_form_args);
    jQuery("#date_of_purchase").datepicker();
    
     var product_form_rules={
        'category_box' :{
               'required' : true,
        },
        'company_box' :{
               'required' : true,
        },
        'product_title' :{
               'required' : true,
                noSpace: true
        },
        'product_imei' :{
               'required' : true,
                noSpace: true
        },
        'model_number' :{
               'required' : true,
                noSpace: true
        },
        'ram' :{
               'required' : true,
                noSpace: true
        },
        'memory' :{
               'required' : true,
                noSpace: true
        },
        'product_desc' :{
               'required' : true,
                noSpace: true
        },
        'date_of_purchase' :{
               'required' : true,
                noSpace: true
        },
        'purchase_amount' :{
               'required' : true,
                noSpace: true
        },
        'seller_name' :{
               'required' : true,
                noSpace: true
        },
        'seller_address' :{
               'required' : true,
                noSpace: true
        },
        'seller_mobile' :{
               'required' : true,
                noSpace: true
        },
        'seller_id_proof' :{
               'required' : true,
                noSpace: true
        },
    };
    var product_form_msg={
        noSpace:admin_veriables.no_space
    }
    var product_form_args = {
         rules: product_form_rules,
         message:product_form_msg,
            onkeyup: false,
            errorClass: 'hifi-validation-error',
        
     }
     jQuery("#product_form").validate(product_form_args);
     
    var seller_form_rules={
         'seller_name' :{
               'required' : true,
                noSpace: true
        },
        'seller_address' :{
               'required' : true,
                noSpace: true
        },
        'seller_mobile' :{
               'required' : true,
                noSpace: true
        },
        'seller_id_proof' :{
               'required' : true,
                noSpace: true
        },
    };
     var seller_form_msg={
        noSpace:admin_veriables.no_space
    }
    var seller_form_args = {
         rules: seller_form_rules,
         message:seller_form_msg,
            onkeyup: false,
            errorClass: 'hifi-validation-error',
        
     }
     jQuery("#seller_form").validate(seller_form_args);
     
});

jQuery(function () {
    
    
    jQuery(".toplevel_page_hifi_main_menu input[name='select_type']").click(function(){
        jQuery(".toplevel_page_hifi_main_menu input[name='select_type']").removeAttr("checked");
        jQuery(this).attr("checked","checked");
        var val=jQuery(this).val();
        if(val==1)
        {
            clear_seller_info();
            jQuery(".seller_box_div").show();
            jQuery(".seller_add").hide();
        }
        else
        {
            jQuery(".seller_add").show();
            jQuery(".seller_box_div").hide();
            jQuery(".seller_box").val('');
            jQuery(".seller_add").children(".hifi-validation-error").remove();
            
        }
    });
    jQuery(".hifi-plugin_page_hifi_manage_stock input[name='select_type']").click(function(){
        jQuery(".hifi-plugin_page_hifi_manage_stock input[name='select_type']").removeAttr("checked");
        jQuery(this).attr("checked","checked");
        var val=jQuery(this).val();
        if(val==1)
        {
            console.log(1);
            jQuery(".buyer_box_div").show();
             jQuery(".buyer_add").hide();
        }
        else
        {
            jQuery(".buyer_add").show();
            jQuery(".buyer_box_div").hide();
            jQuery(".buyer_box").val('');
            jQuery(".buyer_add").children(".hifi-validation-error").remove();
        }
    });
     var buyer_rules={
        'buyer_name' :{
               'required' : true,
                noSpace: true
        },
        'buyer_address' :{
               'required' : true,
                noSpace: true
        },
        'buyer_mobile' :{
               'required' : true,
                noSpace: true
        },
        'buyer_id_proof' :{
               'required' : true,
                noSpace: true
        },
        'sell_price' :{
               'required' : true,
                noSpace: true
        },
        'sell_date' :{
               'required' : true,
                noSpace: true
        },
        'buyer_remarks' :{
                noSpace: true
        },
     };
     var buyer_message={
         noSpace:admin_veriables.no_space
     };
    
     var buyer_form_args = {
         rules: buyer_rules,
         message:buyer_message,
            onkeyup: false,
            errorClass: 'hifi-validation-error',
        
     }
     jQuery("#buyer_info").validate(buyer_form_args);
     
    jQuery('#buyer_btn').click(function(e){
        $('#buyer_info').submit();    
        return false;
    });
    
    jQuery("#sell_date").datepicker();
    
    jQuery('.buyer_product_status').click(function(){
        console.log(jQuery(this));
        var product_id=jQuery(this).attr("data-product_id");
        console.log(product_id);
        jQuery("#buyer_product_id").val('');
        jQuery("#buyer_product_id").val(product_id);
    });
    
    jQuery(".button-link").click(function (e) {

        if (jQuery(this).parent().hasClass("closed"))
        {
            jQuery(this).parent().removeClass("closed");
            jQuery(this).attr("aria-expanded", "true");
        } else
        {
            jQuery(this).parent().addClass("closed");
            jQuery(this).attr("aria-expanded", "false");
        }
    });
    
    
});
function clear_seller_info()
{
    jQuery("input[name='seller_name']").val("");
    jQuery("input[name='seller_address']").val("");
    jQuery("input[name='seller_mobile_no']").val("");
    jQuery("input[name='seller_id_proof']").val("");
    jQuery("input[name='seller_image']").val("");
//    jQuery(".seller_add").children("#seller_").remove();
}
$(function() {

    jQuery(".report-btn").click(function(){
       var report_type=jQuery("#report_type").val();
       var report_category=jQuery("#report_category").val();
       console.log(report_type);
       console.log(report_category);
       if(report_type != '' && report_category != '')
       {  
           jQuery("#report_error").hide();
           return true;
       }
       else
       { 
           jQuery("#report_error").show();
           jQuery("#report_error").html('');
           jQuery("#report_error").html(admin_veriables.report_error);
           return false;
       }
    });
    
    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        jQuery("#report_start_date").val('');
        jQuery("#report_start_date").val(start.format('YYYY-MM-DD'));
        jQuery("#report_end_date").val('');
        jQuery("#report_end_date").val(end.format('YYYY-MM-DD'));
        jQuery("#report_full_date").val('');
        jQuery("#report_full_date").val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);
    
    jQuery(".hifi-report").DataTable({
        responsive: true,
        "ordering": false,
    });

});