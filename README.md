﻿# Hifi Stock management plugin

### Introduction

Hifi Stock management is a wordpress plugin which allows administrator to maintain a stock and add the product on backend side.

  - Add product
  - Add Seller
  - Add Buyer
  - Generate selling report


### Prerequisites

- To install this plugin, you will need to setup wordpress project in your computer.
- Go to Settings menu of a wordpress dashboard -> Discussion and change default avatar for users without a custom avatar of their own.
- Add new users having role author. Go to USERS -> add new  -> select role 'author'.


### Installing

> **Uploading within WordPress Dashboard**

```
    1. Download the zip of this repository.
    2. Navigate to ‘Add New’ within the plugins dashboard.
    3. Navigate to ‘Upload’ area.
    4. Select ‘zip of plugin files’ from your computer.
    5. Click ‘Install Now’.
    6. Activate the plugin within the Plugin dashboard.
```

> **Using FTP**

```
    1. Extract the ‘zip’ directory to your computer.
    2. Upload the plugin directory to the ‘/wp-content/plugins/’ directory.
    3. Activate the plugin from the Plugin dashboard.
```

### How to use it ?


 - When plugin is activated you can see the Hifi plugin menu
 - From this menu there is lots of inner menu which them you can maintain your product.
 - Also you can generate the sell report of product.

### Author

**Sarfaraj Kazi** 

> **Social Presence**

[linkedin]( https://www.linkedin.com/in/sarfaraj-kazi-8a1325ab/)
