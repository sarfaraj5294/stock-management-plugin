<?php
/*
Plugin Name: Hifi
Plugin URI: 
Description: 
Version: 1.0.0
Author:
Author URI: 
License: GPLv2
TEXT DOMAIN : hifi_domain
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

// Define HIFI_PLUGIN_FILE.
if ( ! defined( 'HIFI_PLUGIN_FILE' ) ) {
	define( 'HIFI_PLUGIN_FILE', __FILE__ );
}

// Include the main HIFI class.
if ( ! class_exists( 'HIFI' ) ) {
	include_once dirname( __FILE__ ) . '/includes/hifi_class.php';
}

/**
 * Main instance of HIFI .
 * 
 * Returns main instance of HIFI  to prevent the need to use globals.
 * 
 * @since 1.0.0
 * @return HIFI
 */
function HIFI_getInstance()
{
    return HIFI::hifi_instance();
}
$GLOBALS['HIFI']= HIFI_getInstance();

function dd($data = false, $flag = false, $display = false) {
       if (empty($display)) {
           echo "<pre>";
           if ($flag == 1) {
               print_r($data);
               die;
           } else {
               print_r($data);
           }
           echo "</pre>";
       } else {
           echo "<div style='display:none'><pre>";
           print_r($data);
           echo "</pre></div>";
       }
   }
