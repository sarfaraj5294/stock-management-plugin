<?php
if (!defined('ABSPATH')) {
    exit;
}

/**
 * Handle all Admin Requests
 *
 * @class hifi_admin
 * @since 1.0.0
 * @author Sarfaraj kazi
 */
class hifi_admin {
 
    /**
     * Constructor.
     */
    public function __construct() {
        add_action('init', array($this, 'hifi_admin_includes'));
    }
 
    /**
     * include admin assests file
     */
    public function hifi_admin_includes() {
        include_once HIFI_PLUGIN_PATH . 'includes/admin/hifi_assets_admin.php';
        include_once HIFI_PLUGIN_PATH . 'includes/admin/hifi_action_handler_admin.php';   
        include_once HIFI_PLUGIN_PATH . 'includes/admin/hifi_init_requests.php';   
    }
}
return new hifi_admin();