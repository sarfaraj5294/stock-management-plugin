<?php

if (!defined('ABSPATH')) {
    exit;
}

/**
 * Handle all Admin Requests
 *
 * @class Hifi_init_requests
 * @since 1.0.0
 * @author Sarfaraj kazi
 */
class Hifi_init_requests {

    /**
     * Constructor.
     */
    public function __construct() {
        add_action('admin_init', array($this, 'hifi_admin_init_request'));
        add_action('admin_init', array($this, 'hifi_save_form_fields'));
    }

    /**
     * Handle admin Init Requests
     */
    public function hifi_admin_init_request() {
        global $wp_roles;
        if (!isset($wp_roles))
            $wp_roles = new WP_Roles();
        $adm = $wp_roles->get_role('subscriber');
        $wp_roles->add_role('manager', 'Manager', $adm->capabilities);
    }

    function hifi_save_form_fields() {
        global $wpdb;
        $wp_nonne = isset($_POST['_wpnonce']) ? $_POST['_wpnonce'] : "";
        if (wp_verify_nonce($wp_nonne, 'manage_category')) {

            $edit_id = isset($_POST['edit_id']) ? $_POST['edit_id'] : 0;

            $category_title = $_POST['category_title'];
            $category_desc = $_POST['category_desc'];
            $save = isset($_POST['save_category']) ? $_POST['save_category'] : "";
            $save_and_close = isset($_POST['save_close_category']) ? $_POST['save_close_category'] : "";
            $insert_array = array('category_name' => $category_title, 'category_desc' => $category_desc);

            if ($edit_id) {
                $insert_array['category_updated_date'] = date_i18n(HIFI_DB_DATE_FORMAT);
                $wpdb->update(HIFI_CATEGORY_TBL, $insert_array, array('category_id' => $edit_id));
            } else {
                $insert_array['categoy_updated'] = date(HIFI_DB_DATE_FORMAT);
                $insert_db = $wpdb->insert(HIFI_CATEGORY_TBL, $insert_array);
            }



            if ($save) {
                $url = HIFI_ADMIN_URL . 'admin.php?page=hifi_manage_module&manage_rec=1&manage_type=add';
            } elseif ($save_and_close) {
                $url = HIFI_ADMIN_URL . 'admin.php?page=hifi_manage_module';
            }
            wp_safe_redirect($url);
            die();
        }
        if (wp_verify_nonce($wp_nonne, 'manage_company')) {
            $edit_id = isset($_POST['edit_id']) ? $_POST['edit_id'] : 0;
            $company_title = $_POST['company_title'];
            $company_desc = $_POST['company_desc'];
            $save = isset($_POST['save_company']) ? $_POST['save_company'] : "";
            $save_and_close = isset($_POST['save_close_company']) ? $_POST['save_close_company'] : "";
            $insert_array = array('company_name' => $company_title, 'company_desc' => $company_desc);
            if ($edit_id) {
                $insert_array['company_updated_date'] = date_i18n(HIFI_DB_DATE_FORMAT);
                $wpdb->update(HIFI_COMPANY_TBL, $insert_array, array('company_id' => $edit_id));
            } else {
                $wpdb->insert(HIFI_COMPANY_TBL, $insert_array);
            }
            if ($save) {
                $url = HIFI_ADMIN_URL . 'admin.php?page=hifi_manage_company&manage_rec=1&manage_type=add';
            } elseif ($save_and_close) {
                $url = HIFI_ADMIN_URL . 'admin.php?page=hifi_manage_company';
            }
            wp_safe_redirect($url);
            die();
        }
        if (wp_verify_nonce($wp_nonne, 'manage_product')) {
            $edit_id = isset($_POST['edit_id']) ? $_POST['edit_id'] : 0;
            $seller_photo_id = isset($_POST['seller_photo_id']) ? $_POST['seller_photo_id'] : 0;
            if ($_POST['select_type'] == 1) {
                $seller_id = $_POST['seller_id'];
            } elseif ($_POST['select_type'] == 2) {
                $seller_photo_attachment_id = 0;
                if ($_FILES) {
                    $seller_photo = isset($_FILES['seller_image']) ? $_FILES['seller_image'] : array();
                    if (!empty($seller_photo)) {
                        if ($seller_photo['size'] != 0) {
                            $seller_photo_file = $seller_photo['tmp_name'];
                            $seller_photo_filename = $seller_photo['name'];
                            $seller_photo_upload_file = wp_upload_bits($seller_photo_filename, null, file_get_contents($seller_photo_file));
                            try {
                                if (!$seller_photo_upload_file['error']) {
                                    $seller_photo_wp_filetype = wp_check_filetype($seller_photo_filename, null);
                                    $seller_photo_attachment = array(
                                        'post_mime_type' => $seller_photo_upload_file['type'],
                                        'post_title' => preg_replace('/\.[^.]+$/', '', $seller_photo_filename),
                                        'post_content' => '',
                                        'post_status' => 'inherit'
                                    );
                                    if ($seller_photo_id && $seller_photo_id != 0) {
                                        wp_delete_attachment($seller_photo_id, true);
                                    }
                                    $seller_photo_attachment_id = wp_insert_attachment($seller_photo_attachment, $seller_photo_upload_file['file']);
                                    if (!is_wp_error($seller_photo_attachment_id)) {
                                        require_once(ABSPATH . "wp-admin" . '/includes/image.php');
                                        $attachment_data = wp_generate_attachment_metadata($seller_photo_attachment_id, $seller_photo_upload_file['file']);
                                        wp_update_attachment_metadata($seller_photo_attachment_id, $attachment_data);
                                        $seller_photo_upload_url = wp_get_attachment_url($seller_photo_attachment_id);
                                    }
                                } else {
                                    throw new Exception(__('Image not Upload. ' . $seller_photo_upload_file['error'], 'membership2'), 1);
                                    return;
                                }
                            } catch (Exception $ex) {
                                $_SESSION['err'] = __('Image not Upload. ' . $seller_photo_upload_file['error'], 'membership2');
                                return;
                            }
                        }
                    }
                }

                $seller_insert_array = array(
                    'seller_name' => $_POST['seller_name'],
                    'seller_address' => $_POST['seller_address'],
                    'seller_mobile_no' => $_POST['seller_mobile_no'],
                    'seller_id_proof' => $_POST['seller_id_proof'],
                    'seller_image' => $seller_photo_attachment_id,
                );
                $seller_id = $wpdb->insert(HIFI_SELLER_TBL, $seller_insert_array);
            }

            $product_other_info_arr = array(
                'product_ram' => $_POST['product_ram'],
                'product_memory' => $_POST['product_memory'],
            );

            $product_other_info = serialize($product_other_info_arr);
            $product_insert_array = array(
                'category_id' => $_POST['category_id'],
                'company_id' => $_POST['company_id'],
                'seller_id' => $seller_id,
                'product_IMEI' => $_POST['product_IMEI'],
                'product_model_number' => $_POST['product_model_number'],
                'product_title' => $_POST['product_title'],
                'product_description' => $_POST['product_description'],
                'product_other_info' => $product_other_info,
                'date_of_purchase' => date_i18n(HIFI_DB_DATE_FORMAT, strtotime($_POST['date_of_purchase'])),
                'purchase_amount' => $_POST['purchase_amount'],
                'product_status' => HIFI_IN_STOCK,
            );

            if ($edit_id) {
                $product_insert_array['product_updated_date'] = date_i18n(HIFI_DB_DATE_FORMAT);
                $wpdb->update(HIFI_PRODUCT_TBL, $product_insert_array, array('product_id' => $edit_id));
            } else {
                $product_id = $wpdb->insert(HIFI_PRODUCT_TBL, $product_insert_array);
            }



            $save = isset($_POST['save_product']) ? $_POST['save_product'] : "";
            $save_and_close = isset($_POST['save_close_product']) ? $_POST['save_close_product'] : "";
            if ($save) {
                $url = HIFI_ADMIN_URL . 'admin.php?page=hifi_manage_product';
            } elseif ($save_and_close) {
                $url = HIFI_ADMIN_URL . 'admin.php?page=hifi_manage_stock';
            }
            wp_safe_redirect($url);
            die();
        }
        if (wp_verify_nonce($wp_nonne, 'buyer_info')) {

            $buyer_product_id = $_POST['buyer_product_id'];
            if (isset($_POST['select_type']) && $_POST['select_type'] == 1) {
                $buyer_id = $_POST['buyer_id'];
                $old_buyer_data = $wpdb->get_row("SELECT * FROM " . HIFI_BUYER_TBL . ' WHERE buyer_id=' . $buyer_id, ARRAY_A);
                $buyer_insert_array = array(
                    'buyer_name' => $old_buyer_data['buyer_name'],
                    'product_id' => $buyer_product_id,
                    'buyer_address' => $old_buyer_data['buyer_address'],
                    'buyer_mobile_no' => $old_buyer_data['buyer_mobile_no'],
                    'buyer_id_proof' => $old_buyer_data['buyer_id_proof'],
                    'sell_price' => $_POST['sell_price'],
                    'sell_date' => date_i18n(HIFI_DB_DATE_FORMAT, strtotime($_POST['sell_date'])),
                    'buyer_reference_person' => $_POST['buyer_reference_person'],
                    'buyer_remarks' => $_POST['buyer_remarks'],
                );
            } elseif (isset($_POST['select_type']) && $_POST['select_type'] == 2) {
                $buyer_insert_array = array(
                    'buyer_name' => $_POST['buyer_name'],
                    'product_id' => $buyer_product_id,
                    'buyer_address' => $_POST['buyer_address'],
                    'buyer_mobile_no' => $_POST['buyer_mobile_no'],
                    'buyer_id_proof' => $_POST['buyer_id_proof'],
                    'sell_price' => $_POST['sell_price'],
                    'sell_date' => date_i18n(HIFI_DB_DATE_FORMAT, strtotime($_POST['sell_date'])),
                    'buyer_reference_person' => $_POST['buyer_reference_person'],
                    'buyer_remarks' => $_POST['buyer_remarks'],
                );
            }
            $wpdb->insert(HIFI_BUYER_TBL, $buyer_insert_array);
            $buyer_id = $wpdb->insert_id;


            $product_tbl_data = array(
                'buyer_id' => $buyer_id,
                'product_status' => HIFI_SOLD,
                'date_of_sell' => date_i18n(HIFI_DB_DATE_FORMAT, strtotime($_POST['sell_date'])),
            );

            $updated_rec = $wpdb->update(HIFI_PRODUCT_TBL, $product_tbl_data, array('product_id' => $buyer_product_id));
            wp_safe_redirect(HIFI_ADMIN_URL . 'admin.php/?page=hifi_manage_stock');
            die;
        }
        if (wp_verify_nonce($wp_nonne, 'manage_seller')) {
            $edit_id = isset($_POST['edit_id']) ? $_POST['edit_id'] : 0;

            $seller_photo_id = isset($_POST['seller_photo_id']) ? $_POST['seller_photo_id'] : 0;
            if ($_FILES) {
                $seller_photo = isset($_FILES['seller_image']) ? $_FILES['seller_image'] : array();

                if (!empty($seller_photo)) {
                    if ($seller_photo['size'] != 0) {
                        $seller_photo_file = $seller_photo['tmp_name'];
                        $seller_photo_filename = $seller_photo['name'];
                        $seller_photo_upload_file = wp_upload_bits($seller_photo_filename, null, file_get_contents($seller_photo_file));
                        try {
                            if (!$seller_photo_upload_file['error']) {
                                $seller_photo_wp_filetype = wp_check_filetype($seller_photo_filename, null);
                                $seller_photo_attachment = array(
                                    'post_mime_type' => $seller_photo_upload_file['type'],
                                    'post_title' => preg_replace('/\.[^.]+$/', '', $seller_photo_filename),
                                    'post_content' => '',
                                    'post_status' => 'inherit'
                                );
                                if ($seller_photo_id && $seller_photo_id != 0) {
                                    wp_delete_attachment($seller_photo_id, true);
                                }
                                $seller_photo_id = wp_insert_attachment($seller_photo_attachment, $seller_photo_upload_file['file']);
                                if (!is_wp_error($seller_photo_id)) {
                                    require_once(ABSPATH . "wp-admin" . '/includes/image.php');
                                    $attachment_data = wp_generate_attachment_metadata($seller_photo_id, $seller_photo_upload_file['file']);
                                    wp_update_attachment_metadata($seller_photo_id, $attachment_data);
                                    $seller_photo_upload_url = wp_get_attachment_url($seller_photo_id);
                                }
                            } else {
                                throw new Exception(__('Image not Upload. ' . $seller_photo_upload_file['error'], 'membership2'), 1);
                                return;
                            }
                        } catch (Exception $ex) {
                            $_SESSION['err'] = __('Image not Upload. ' . $seller_photo_upload_file['error'], 'membership2');
                            return;
                        }
                    }
                }
            }

            $seller_insert_array = array(
                'seller_name' => $_POST['seller_name'],
                'seller_address' => $_POST['seller_address'],
                'seller_mobile_no' => $_POST['seller_mobile_no'],
                'seller_id_proof' => $_POST['seller_id_proof'],
                'seller_image' => $seller_photo_id,
            );

            if ($edit_id) {
                $seller_insert_array['seller_updated_date'] = date_i18n(HIFI_DB_DATE_FORMAT);
                $wpdb->update(HIFI_SELLER_TBL, $seller_insert_array, array('seller_id' => $edit_id));
            } else {
                $seller_id = $wpdb->insert(HIFI_SELLER_TBL, $seller_insert_array);
            }
            $save = isset($_POST['save_seller']) ? $_POST['save_seller'] : "";
            $save_and_close = isset($_POST['save_close_seller']) ? $_POST['save_close_seller'] : "";
            if ($save) {
                $url = HIFI_ADMIN_URL . 'admin.php?page=hifi_manage_seller&manage_rec=1&manage_type=add';
            } elseif ($save_and_close) {
                $url = HIFI_ADMIN_URL . 'admin.php?page=hifi_manage_seller';
            }
            wp_safe_redirect($url);
            die();
        }
        if (wp_verify_nonce($wp_nonne, 'manage_buyer_rec')) {
            $edit_id = isset($_POST['edit_id']) ? $_POST['edit_id'] : 0;
            $buyer_insert_array = array(
                'buyer_name' => $_POST['buyer_name'],
                'buyer_address' => $_POST['buyer_address'],
                'buyer_mobile_no' => $_POST['buyer_mobile_no'],
                'buyer_id_proof' => $_POST['buyer_id_proof'],
                'sell_price' => $_POST['sell_price'],
                'sell_date' => date_i18n(HIFI_DB_DATE_FORMAT, strtotime($_POST['sell_date'])),
                'buyer_reference_person' => $_POST['buyer_reference_person'],
                'buyer_remarks' => $_POST['buyer_remarks'],
            );
             if ($edit_id) {
                $buyer_insert_array['buyer_updated_date'] = date_i18n(HIFI_DB_DATE_FORMAT);
                $wpdb->update(HIFI_BUYER_TBL, $buyer_insert_array, array('buyer_id' => $edit_id));
            }
            else
            {
                  $wpdb->insert(HIFI_BUYER_TBL, $buyer_insert_array);
            }
            
            wp_safe_redirect(HIFI_ADMIN_URL . 'admin.php/?page=hifi_manage_buyer');
            die;
        }
        
        
    }

}

return new Hifi_init_requests();
