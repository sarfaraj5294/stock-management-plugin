<?php
if (!class_exists('WP_List_Table')) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class rec_list_company extends wp_list_table {
    
    protected $table_name=null;
    function __construct($table_name) {
        
        
        parent::__construct(array(
            'singular' => __('Company', "hifi_domain"),
            'plural' => __('Companies', "hifi_domain"),
            'ajax' => false
        ));
        //$this->items = self::get_rec( 5, 1 );
    }

    public static function get_rec($per_page = 10, $page_number = 1) {
        global $wpdb;
        
        $sql = "SELECT * FROM `" . HIFI_COMPANY_TBL .'`';
        if (!empty($_REQUEST['orderby'])) {
             $sql .= ' ORDER BY ' . esc_sql($_REQUEST['orderby']);
            $sql .= !empty($_REQUEST['order']) ? ' ' . esc_sql($_REQUEST['order']) : 'DESC';
        }
        else
        {
            $sql .= ' ORDER BY company_created_date  DESC';
        }
        $sql .= " LIMIT $per_page";
        $sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;
        
        $result = $wpdb->get_results($sql, 'ARRAY_A');
        return $result;
    }

    public static function delete_rec($id) {
        global $wpdb;
        $wpdb->delete(
                HIFI_COMPANY_TBL, ['company_id' => $id], ['%d']
        );
    }

    public static function record_count() {
        global $wpdb;
        
       $sql = "SELECT COUNT(*) FROM `" . HIFI_COMPANY_TBL .'`';
       
        return $wpdb->get_var($sql);
    }

    public function no_items() {
        _e('No Records.', "hifi_domain");
    }

    function column_Name($item) {

        $delete_nonce = wp_create_nonce('hifi_delete_rec_company');
        $id = $item['company_id'];
        $actions = [
            'delete' => sprintf('<a href="?page=%s&action=%s&company_id=%s&_wpnonce=%s">'.__("Delete","hifi_domain").'</a>', esc_attr($_REQUEST['page']), 'delete', absint($item['company_id']), $delete_nonce)
        ];
        return $id . $this->row_actions($actions);
    }

    public function column_default($item, $column_name) {
        switch ($column_name) {
           
            case 'company_id':
                return $item[$column_name];
            case 'company_name':
                return $item[$column_name];
            case 'company_created_date':
                return date(HIFI_DATE_FORMAT.' '.HIFI_TIME_FORMAT, strtotime($item[$column_name]));
            case 'company_updated_date':
                return date(HIFI_DATE_FORMAT.' '.HIFI_TIME_FORMAT, strtotime($item[$column_name]));
            default:
                return print_r($item, true);
        }
    }

    function column_cb($item) {
        return sprintf('<input type="checkbox" name="bulk-delete[]" value="%s" />', $item['company_id']);
    }
    function column_action($item) {
            return sprintf('<a href="%s">'.__("Edit","hifi_domain").'</a>', esc_url(HIFI_ADMIN_URL . 'admin.php?page=hifi_manage_company&manage_rec=1&manage_type=edit&edit_id='.$item['company_id']));
        }
    function get_columns() {
        $columns = array(
                'cb' => '<input type="checkbox" name="chkid" />',
                'company_id' => __('ID', "hifi_domain"),
                'company_name' => __('Company Name', "hifi_domain"),
                'company_created_date' => __('Created on', "hifi_domain"),
                'company_updated_date' => __('Modify on', "hifi_domain"),
                'action' => __('Action', "hifi_domain"),
            );
        
        return $columns;
    }

    public function get_sortable_columns() {
        $sortable_columns = array(
            'company_id' => array('company_id', true),
            'company_created_date' => array('company_created_date', true),
            'company_updated_date' => array('company_updated_date', true),

        );
        return $sortable_columns;
    }

    public function get_bulk_actions() {
        $actions = [
            'bulk-delete' => 'Delete'
        ];
        return $actions;
    }

    public function prepare_items() {
        $this->_column_headers = array($this->get_columns(), array(), $this->get_sortable_columns());
        /** Process bulk action */
        $this->process_bulk_action();

        $per_page = $this->get_items_per_page('rec_per_page', 10);
        $current_page = $this->get_pagenum();
        $total_items = self::record_count();

        $this->set_pagination_args([
            'total_items' => $total_items, //WE have to calculate the total number of items
            'per_page' => $per_page //WE have to determine how many items to show on a page
        ]);

        $this->items = self::get_rec($per_page, $current_page);
    }

    public function process_bulk_action() {
        if ('delete' === $this->current_action()) {
            $nonce = esc_attr($_REQUEST['_wpnonce']);
            if (!wp_verify_nonce($nonce, 'hifi_delete_rec_company')) {
                die('Go get a life script kiddies');
            } else {
                self::delete_rec(absint($_GET['company_id']));
            }
        }
        if (( isset($_POST['action']) && $_POST['action'] == 'bulk-delete' ) || ( isset($_POST['action2']) && $_POST['action2'] == 'bulk-delete' )
        ) {
            $delete_ids = esc_sql($_POST['bulk-delete']);
            foreach ($delete_ids as $id) {
                self::delete_rec($id);
            }
        }
    }

}
