<?php

if (!class_exists('WP_List_Table')) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class rec_list_product extends wp_list_table {

    protected $table_name = null;

    function __construct($table_name) {


        parent::__construct(array(
            'singular' => __('Product', ""),
            'plural' => __('Products', "hifi_domain"),
            'ajax' => false
        ));
        //$this->items = self::get_rec( 5, 1 );
    }

    protected function get_views() { 
    $status_links = array(
        "all"       => __("<a href='".HIFI_ADMIN_URL . 'admin.php?page=hifi_manage_stock'."'>All</a>",'hifi_domain'),
        "in_stock" => __("<a href='".HIFI_ADMIN_URL . 'admin.php?page=hifi_manage_stock&product_slug=in_stock'."'>In Stock</a>",'hifi_domain'),
        "sold"   => __("<a href='".HIFI_ADMIN_URL . 'admin.php?page=hifi_manage_stock&product_slug=sold'."'>Sold</a>",'hifi_domain')
    );
    return $status_links;
}

    
    public static function get_rec($per_page = 10, $page_number = 1) {
        global $wpdb;
        $bool=0;
        $sql = "SELECT * FROM `" . HIFI_PRODUCT_TBL . '`';
       
        if(isset($_GET['cat-filter']) && $_GET['cat-filter'] > 0 ){
            $sql .=' JOIN '.HIFI_CATEGORY_TBL.' ON '.HIFI_PRODUCT_TBL.'.category_id = '.HIFI_CATEGORY_TBL.'.category_id';
            $sql .= ' where '.HIFI_PRODUCT_TBL.'.category_id=' . $_GET['cat-filter'];   
        }
        if(isset($_GET['company-filter']) && $_GET['company-filter'] > 0 ){
            $sql .=' JOIN '.HIFI_COMPANY_TBL.' ON '.HIFI_PRODUCT_TBL.'.company_id = '.HIFI_COMPANY_TBL.'.company_id';
            $sql .= ' where '.HIFI_PRODUCT_TBL.'.company_id=' . $_GET['company-filter'];
        }
        
        if (isset($_REQUEST['s']) && !empty($_REQUEST['s'])) {
            $sql .=' JOIN '.HIFI_CATEGORY_TBL.' ON '.HIFI_PRODUCT_TBL.'.category_id = '.HIFI_CATEGORY_TBL.'.category_id';
            $sql .=' JOIN '.HIFI_COMPANY_TBL.' ON '.HIFI_PRODUCT_TBL.'.company_id = '.HIFI_COMPANY_TBL.'.company_id ';
            $sql .=' JOIN '.HIFI_SELLER_TBL.' ON '.HIFI_PRODUCT_TBL.'.seller_id = '.HIFI_SELLER_TBL.'.seller_id ';
            $sql .=' JOIN '.HIFI_BUYER_TBL.' ON '.HIFI_PRODUCT_TBL.'.buyer_id = '.HIFI_BUYER_TBL.'.buyer_id ';
            $sql .= " WHERE (".HIFI_PRODUCT_TBL.".product_title LIKE '%{$_REQUEST['s']}%' OR ".HIFI_PRODUCT_TBL.".product_description LIKE '%{$_REQUEST['s']}%' OR ".HIFI_PRODUCT_TBL.".product_IMEI LIKE '%{$_REQUEST['s']}%' OR ".HIFI_PRODUCT_TBL.".product_model_number LIKE '%{$_REQUEST['s']}%' OR ".HIFI_PRODUCT_TBL.".purchase_amount LIKE '%{$_REQUEST['s']}%' OR ".HIFI_CATEGORY_TBL.".category_name LIKE '%{$_REQUEST['s']}%' OR ".HIFI_COMPANY_TBL.".company_name LIKE '%{$_REQUEST['s']}%' OR ".HIFI_SELLER_TBL.".seller_name LIKE '%{$_REQUEST['s']}%'  OR ".HIFI_BUYER_TBL.".buyer_name LIKE '%{$_REQUEST['s']}%' )";
            $bool=1;
        }
         if(isset($_REQUEST['product_slug']) && !empty($_REQUEST['product_slug']))
        {
             if($bool==1)
             {
                 $slg=" AND ";
             }
             else
                 $slg=" WHERE ";
             
            if($_REQUEST['product_slug']=='in_stock')
            {
                
                $sql.=$slg."product_status=1";
            }
            elseif($_REQUEST['product_slug']=='sold')
            {
                $sql.=$slg."product_status=0";
            }
        }
        if (!empty($_REQUEST['orderby'])) {

            $sql .= !empty($_REQUEST['order']) ? ' ' . esc_sql($_REQUEST['order']) : 'DESC';
        } else {
            $sql .= ' ORDER BY product_created_date  DESC';
        }

        $sql .= " LIMIT $per_page";
        $sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;

        $result = $wpdb->get_results($sql, 'ARRAY_A');
        return $result;
    }

    public static function delete_rec($id) {
        global $wpdb;
        $wpdb->delete(
                HIFI_PRODUCT_TBL, ['product_id' => $id], ['%d']
        );
    }

    public static function record_count() {
        global $wpdb;

        $sql = "SELECT COUNT(*) FROM `" . HIFI_PRODUCT_TBL . '`';

        return $wpdb->get_var($sql);
    }

    public function no_items() {
        _e('No Records.', "hifi_domain");
    }

    function column_Name($item) {

        $delete_nonce = wp_create_nonce('hifi_delete_rec_product');
        $id = $item['product_id'];
        $actions = [
            'delete' => sprintf('<a href="?page=%s&action=%s&product_id=%s&_wpnonce=%s">' . __("Delete", "hifi_domain") . '</a>', esc_attr($_REQUEST['page']), 'delete', absint($item['product_id']), $delete_nonce)
        ];
        return $id . $this->row_actions($actions);
    }

    public function column_default($item, $column_name) {
        global $wpdb;
        switch ($column_name) {

            case 'product_id':
                return $item[$column_name];
            case 'product_title':
                return $item[$column_name];
            case 'category_id':
                $category_data = $wpdb->get_row("SELECT `category_name` FROM " . HIFI_CATEGORY_TBL . " WHERE category_id=" . $item[$column_name]);
                return $category_data->category_name;
            case 'company_id':
                $company_data = $wpdb->get_row("SELECT `company_name` FROM " . HIFI_COMPANY_TBL . " WHERE company_id=" . $item[$column_name]);
                return $company_data->company_name;
            case 'seller_id':

                $seller_data = $wpdb->get_row("SELECT `seller_name` FROM " . HIFI_SELLER_TBL . " WHERE seller_id=" . $item[$column_name]);
                if ($seller_data)
                    return $seller_data->seller_name;
                else
                    return '-';
            case 'buyer_id':
                $buyer_data="";
                if($item[$column_name])
                    $buyer_data = $wpdb->get_row("SELECT `buyer_name` FROM " . HIFI_BUYER_TBL . " WHERE buyer_id=" . $item[$column_name]);
                
                if ($buyer_data)
                    return $buyer_data->buyer_name;
                else
                    return '-';
            case 'product_IMEI':
                return $item[$column_name];
            case 'purchase_amount':
                return $item[$column_name];
            case 'product_status':
                if ($item[$column_name] == HIFI_SOLD) {
                    return '<label class="switch"><input disabled data-product_id="' . $item['product_id'] . '" type="checkbox" checked name="product_status" class="product_status"><span class="slider round"></span></label>'
                            . '<span class="label success">' . __("Sold", "hifi_domain") . '</span>';
                } else {
                    return '<label class="switch"><input  type="checkbox" name="product_status" class="product_status"><a href="#TB_inline?width=650&height=550&inlineId=product_buyer_info" class="thickbox"><span data-product_id="' . $item['product_id'] . '" class="slider round buyer_product_status"></span></a></label>'
                            . '<span class="label default">' . __("In Stock", "hifi_domain") . '</span>';
                }
                return $item[$column_name];
            case 'date_of_purchase':
                return date(HIFI_DATE_FORMAT, strtotime($item[$column_name]));
            case 'product_updated_date':
                return date(HIFI_DATE_FORMAT . ' ' . HIFI_TIME_FORMAT, strtotime($item[$column_name]));
            default:
                return print_r($item, true);
        }
    }

    function column_cb($item) {
        return sprintf('<input type="checkbox" name="bulk-delete[]" value="%s" />', $item['product_id']);
    }

    function column_action($item) {
        return sprintf('<a href="%s">'.__("Edit","hifi_domain").'</a>', esc_url(HIFI_ADMIN_URL . 'admin.php?page=hifi_main_menu&manage_rec=1&manage_type=edit&edit_id=' . $item['product_id']));
    }

    function get_columns() {
        $columns = array(
            'cb' => '<input type="checkbox" name="chkid" />',
            'product_id' => __('ID', "hifi_domain"),
            'product_title' => __('Name', "hifi_domain"),
            'category_id' => __('Category Name', "hifi_domain"),
            'company_id' => __('Company Name', "hifi_domain"),
            'seller_id' => __('Seller name', "hifi_domain"),
            'buyer_id' => __('Buyer name', "hifi_domain"),
            'product_IMEI' => __('Product IMEI', "hifi_domain"),
            'purchase_amount' => __('Price', "hifi_domain"),
            'product_status' => __('Status', "hifi_domain"),
            'date_of_purchase' => __('Purchase on', "hifi_domain"),
            'product_updated_date' => __('Modify on', "hifi_domain"),
            'action' => __('Action', "hifi_domain"),
        );

        return $columns;
    }

    public function get_sortable_columns() {
        $sortable_columns = array(
            'product_id' => array('product_id', true),
            'date_of_purchase' => array('date_of_purchase', true),
            'product_updated_date' => array('product_updated_date', true),
        );
        return $sortable_columns;
    }

    public function get_bulk_actions() {
        $actions = [
            'bulk-delete' => 'Delete'
        ];
        return $actions;
    }

    public function prepare_items() {
        $this->_column_headers = array($this->get_columns(), array(), $this->get_sortable_columns());
        /** Process bulk action */
        $this->process_bulk_action();

        $per_page = $this->get_items_per_page('rec_per_page', 10);
        $current_page = $this->get_pagenum();
        $total_items = self::record_count();

        $this->set_pagination_args([
            'total_items' => $total_items, //WE have to calculate the total number of items
            'per_page' => $per_page //WE have to determine how many items to show on a page
        ]);

        $this->items = self::get_rec($per_page, $current_page);
    }

    public function process_bulk_action() {
        if ('delete' === $this->current_action()) {
            $nonce = esc_attr($_REQUEST['_wpnonce']);
            if (!wp_verify_nonce($nonce, 'hifi_delete_rec_product')) {
                die('Go get a life script kiddies');
            } else {
                self::delete_rec(absint($_GET['product_id']));
            }
        }
        if (( isset($_POST['action']) && $_POST['action'] == 'bulk-delete' ) || ( isset($_POST['action2']) && $_POST['action2'] == 'bulk-delete' )
        ) {
            $delete_ids = esc_sql($_POST['bulk-delete']);
            foreach ($delete_ids as $id) {
                self::delete_rec($id);
            }
        }
    }

}
