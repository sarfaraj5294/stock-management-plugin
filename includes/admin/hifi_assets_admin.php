<?php

if (!defined('ABSPATH')) {
    exit;
}
/**
 * Handle Front Scripts and StyleSheets
 * @class Hifi_admin_assets
 * @since 1.0.0
 */
if (!class_exists('Hifi_admin_assets', false)) :

    class Hifi_admin_assets {

        /**
         * Hook in tabs.
         */
        public function __construct() {
            $current_role = '';
            $user = wp_get_current_user();
            if (isset($user->roles[0])) {
                $current_role = $user->roles[0];
            }
            if ($current_role == 'manager' || $current_role == 'administrator') {
                add_action('admin_menu', array($this, 'hifi_add_custom_link_into_appearnace_menu'));
            }
            if ($current_role == 'manager') {
                add_action('wp_dashboard_setup', array($this, 'hifi_remove_widgets'));
            }
           
        }

         public function hifi_remove_widgets() {
            global $wp_meta_boxes;
            $wp_meta_boxes['dashboard']['normal']['core'] = array();
            $wp_meta_boxes['dashboard']['side']['core'] = array();
        }

        public function hifi_add_custom_link_into_appearnace_menu() {

            $admin_assets_obj=new Hifi_action_handler_admin();
            
            $hifi_page[] = add_menu_page(__('Hifi Plugin', "hifi_domain"), __('Hifi Plugin', "hifi_domain"), 'read', 'hifi_main_menu', array($admin_assets_obj, 'hifi_add_new_product'), 'dashicons-clipboard', 45);
           $hifi_page[]= add_submenu_page('hifi_main_menu', __('Add New Product', 'hifi_domain'), __('Add New Product', 'hifi_domain'), 'read', 'hifi_main_menu');
            $hifi_page[]=add_submenu_page('hifi_main_menu', __('Stock Management', 'hifi_domain'), __('Stock Management', 'hifi_domain'), 'read', 'hifi_manage_stock', array($admin_assets_obj, 'hifi_manage_stock'));
//            add_submenu_page('hifi_main_menu', __('Inventory Status', 'hifi_domain'), __('Inventory Status', 'hifi_domain'), 'read', 'hifi_inventory_status', array($this, 'hifi_inventory_status'));
//            
//            add_submenu_page('hifi_main_menu', __('Search', 'hifi_domain'), __('Search', 'hifi_domain'), 'read', 'hifi_search', array($this, 'hifi_search'));
//            add_menu_page(__('Manage Modules', "hifi_domain"), __('Manage Modules', "hifi_domain"), 'read', 'hifi_manage_module', array($this, 'hifi_manage_module'), 'dashicons-admin-generic', 46);
           $hifi_page[]= add_submenu_page('hifi_main_menu', __('Manage Category', 'hifi_domain'), __('Manage Category', 'hifi_domain'), 'read', 'hifi_manage_module', array($admin_assets_obj, 'hifi_manage_module'));
            $hifi_page[]=add_submenu_page('hifi_main_menu', __('Manage Company', 'hifi_domain'), __('Manage Company', 'hifi_domain'), 'read', 'hifi_manage_company', array($admin_assets_obj, 'hifi_manage_company'));
            $hifi_page[] =add_submenu_page('hifi_main_menu', __('Manage Seller', 'hifi_domain'), __('Manage Seller', 'hifi_domain'), 'read', 'hifi_manage_seller', array($admin_assets_obj, 'hifi_manage_seller'));
            $hifi_page[] =add_submenu_page('hifi_main_menu', __('Manage Buyer', 'hifi_domain'), __('Manage Buyer', 'hifi_domain'), 'read', 'hifi_manage_buyer', array($admin_assets_obj, 'hifi_manage_buyer'));
            $hifi_page[]=add_submenu_page('hifi_main_menu', __('Report', 'hifi_domain'), __('Report', 'hifi_domain'), 'read', 'hifi_report', array($admin_assets_obj, 'hifi_report'));
            if($hifi_page)
            {
                foreach($hifi_page as $single_menu)
                {
                    add_action( 'load-' . $single_menu, array($this,'hifi_load_admin_js') );
                }
            }

        }
        function hifi_load_admin_js(){
        add_action('admin_enqueue_scripts', array($this, 'hifi_admin_styles'));
        add_action('admin_enqueue_scripts', array($this, 'hifi_admin_scripts'));
    }
        /**
         * Enqueue styles.
         */
        public function hifi_admin_styles() {
            $cssVersion = filemtime(HIFI_PLUGIN_PATH . 'assets/css/hifi_admin_custom_css.css');
            wp_enqueue_style('hifi_custom-style', HIFI_PLUGIN_URL . 'assets/css/hifi_admin_custom_css.css', array(), $cssVersion);
            wp_enqueue_style('font-awsome_css', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css');
            wp_enqueue_style('daterangepickerCss', 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.min.css');
             wp_enqueue_style('jquery-dataTables', 'https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css');
        }

        /**
         * Enqueue scripts.
         */
        public function hifi_admin_scripts() {
            wp_enqueue_script('jquery');
            wp_enqueue_script('jquery-ui-core');
            wp_enqueue_script('jquery-ui-datepicker', array('jquery'));
            wp_enqueue_script('hifi_custom-js-validation', 'https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js', array('jquery'),false,true);
            wp_enqueue_script('daterangepickerMoment', 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/moment.min.js', array('jquery'),false,true);
            wp_enqueue_script('daterangepickerJs', 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.min.js', array('jquery'),false,true);
             wp_enqueue_script('jquery-dataTables-js','https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',array(),false,true);
            //wp_enqueue_script('jquery-ui-self', 'https://code.jquery.com/ui/1.10.3/jquery-ui.js', array('jquery'));
            $jsVersion = filemtime(HIFI_PLUGIN_PATH . 'assets/js/hifi_admin_custom.js');
            wp_register_script('hifi_custom-js-file', HIFI_PLUGIN_URL . 'assets/js/hifi_admin_custom.js', array('jquery'), $jsVersion, true);
            wp_enqueue_script("hifi_custom-js-file");
            wp_localize_script('hifi_custom-js-file', 'admin_veriables', array('ajax_url' => HIFI_ADMIN_AJAX_URL, 'home_url' => HIFI_HOME_URL, 'site_url' => HIFI_SITE_URL,'required_field'=>__("Required field","hifi_domain"),'required_field_chk'=>__("Select atleast one option","hifi_domain"),'required_field_chk_one'=>__("Please select option","hifi_domain"),'email_err_msg'=>__("Enter Valid Email","hifi_domain"),'no_space'=>__("This field is required.","hifi_domain"),'report_error'=>__("Please select type or category","hifi_domain")));
            
            
        }

    }

    endif;
return new Hifi_admin_assets();
