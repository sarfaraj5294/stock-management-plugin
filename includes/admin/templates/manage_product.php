<?php
global $wpdb;
if (isset($_REQUEST['manage_type']) && $_REQUEST['manage_type'] == 'edit') {
    $title = __("Edit", "hifi_domain");
} else {
    $title = __("Add", "hifi_domain");
}
$edit_id=0;
if (isset($_REQUEST['edit_id']) && !empty($_REQUEST['edit_id'])) {
    $edit_id = $_REQUEST['edit_id'];
}
?>
<div class="wrap">
    <h1 class="wp-heading-inline"><?php _e("Manage Product", "hifi_domain"); ?></h1>
    <hr class="wp-header-end">
    <div class="metabox-holder hifi_plugin_page product_page">
        <form method="post" id="product_form" enctype="multipart/form-data">
            <div id="postbox-container-1" class="postbox-container big-container hifi_product_div">
                <div class="meta-box-sortables">
                    <div id="" class="postbox " >
                        <button type="button" class="handlediv button-link" aria-expanded="true"><span class="screen-reader-text">Toggle panel: Getting Started</span><span class="toggle-indicator" aria-hidden="true"></span></button>
                        <h2 class='hndle'><span>  <?php echo $title . ' ' . __("Product", "hifi_domain") ?></span></h2>
                        <div class="inside">
                            <div class="main">

                                <div id="titlediv">
                                    <div class="selectwrap">
                                        <label class="form_input_title"><?php _e("Select product category", "hifi_domain") ?></label>
                                        <select name="category_id"  class="required category_box product_form">
                                            <option value="" hidden><?php _e("Select product category", "hifi_domain") ?></option>
                                            <?php
                                            $category_list = $wpdb->get_results("SELECT * FROM " . HIFI_CATEGORY_TBL);
                                            $category_id=Hifi_action_handler_admin::get_db_post_value(HIFI_PRODUCT_TBL, 'category_id','product_id', $edit_id);
                                            if ($category_list) {
                                                foreach ($category_list as $single_rec) {
                                                    ?>
                                                    <option value="<?php echo $single_rec->category_id ?>" <?php
                                                    if($category_id == $single_rec->category_id )
                                                    {
                                                      echo "selected";  
                                                    }
                                                    ?>><?php echo $single_rec->category_name ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="selectwrap">
                                        <label class="form_input_title"><?php _e("Select product company", "hifi_domain") ?></label>
                                        <select name="company_id"  class="required company_box product_form">
                                            <option value="" hidden><?php _e("Select product company", "hifi_domain") ?></option>
                                            <?php
                                            $company_list = $wpdb->get_results("SELECT * FROM " . HIFI_COMPANY_TBL);
                                            $company_id=Hifi_action_handler_admin::get_db_post_value(HIFI_PRODUCT_TBL, 'company_id','product_id', $edit_id);
                                            if ($company_list) {
                                                foreach ($company_list as $single_rec) {
                                                    ?>
                                                    <option value="<?php echo $single_rec->company_id ?>"<?php
                                                    if($company_id == $single_rec->company_id )
                                                    {
                                                      echo "selected";  
                                                    }
                                                    ?>><?php echo $single_rec->company_name ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div id="titlewrap " class="product_form">
                                        <label class="form_input_title"><?php _e("Name", "hifi_domain") ?></label>
                                        <input type="text" required name="product_title" size="30" placeholder="<?php _e("Enter prodct name", "hifi_domain") ?>" value="<?php echo Hifi_action_handler_admin::get_db_post_value(HIFI_PRODUCT_TBL, 'product_title', 'product_id', $edit_id); ?>" id="product_title" spellcheck="true" autocomplete="off">
                                    </div>
                                    <div id="descwrap" class="product_form">
                                        <label class="form_input_title"><?php _e("Notes", "hifi_domain") ?></label>
                                        <textarea required rows="3" name="product_description" id="product_description" placeholder="<?php _e("Enter Notes here", "hifi_domain") ?>"><?php echo Hifi_action_handler_admin::get_db_post_value(HIFI_PRODUCT_TBL, 'product_description', 'product_id', $edit_id); ?></textarea>
                                    </div>
                                    <div id="imei_wrap" class="product_form">
                                        <label class="form_input_title"><?php _e("IMEI", "hifi_domain") ?></label>
                                        <input type="text" required name="product_IMEI" size="30" placeholder="<?php _e("Product IMEI", "hifi_domain") ?>" value="<?php echo Hifi_action_handler_admin::get_db_post_value(HIFI_PRODUCT_TBL, 'product_IMEI', 'product_id', $edit_id); ?>" id="product_IMEI" spellcheck="true" autocomplete="off">
                                    </div>
                                    <div id="modelno_wrap" class="product_form">
                                        <label class="form_input_title"><?php _e("Model number", "hifi_domain") ?></label>
                                        <input type="text" required name="product_model_number" size="30" placeholder="<?php _e("Model number", "hifi_domain") ?>" value="<?php echo Hifi_action_handler_admin::get_db_post_value(HIFI_PRODUCT_TBL, 'product_model_number', 'product_id', $edit_id); ?>" id="product_model_number" spellcheck="true" autocomplete="off">
                                    </div>
                                    <div id="specification_wrap" class="product_form">
                                        <h3><strong><?php _e("Specification", "hifi_domain"); ?></strong></h3>
                                        
                                        <?php
                                        $specification_rec=Hifi_action_handler_admin::get_db_post_value(HIFI_PRODUCT_TBL, 'product_other_info', 'product_id', $edit_id);
                                        if($specification_rec)
                                        {
                                            $specification_rec= unserialize($specification_rec);
                                        }
                                        ?>
                                        
                                        <label class="form_input_title"><?php _e("RAM", "hifi_domain") ?></label>
                                        <input type="text" required name="product_ram" size="30" placeholder="<?php _e("RAM", "hifi_domain") ?>" value="<?php echo isset($specification_rec['product_ram'])?$specification_rec['product_ram']:"" ?>" id="product_ram" spellcheck="true" autocomplete="off">

                                        <label class="form_input_title"><?php _e("Memory", "hifi_domain") ?></label>
                                        <input type="text" required name="product_memory" size="30" placeholder="<?php _e("Memory", "hifi_domain") ?>" value="<?php echo isset($specification_rec['product_memory'])?$specification_rec['product_memory']:"" ?>" id="product_memory" spellcheck="true" autocomplete="off">
                                    </div>


                                    <div id="seller_wrap" class="product_form">
                                        <label class="form_input_title"><?php _e("Seller Info", "hifi_domain") ?> </label>
                                        <label class="radio_handler"><input type="radio" value="1" name="select_type" checked><?php _e("Select Seller", "hifi_domain") ?> </label>

                                        <label class="radio_handler"><input type="radio" value="2" name="select_type"><?php _e("Add New Seller", "hifi_domain") ?> </label>
                                        <div class="seller_box_div">
                                            <select name="seller_id"  class="required seller_box product_form">
                                                <option value="" hidden><?php _e("Select Seller", "hifi_domain") ?></option>
                                                <?php
                                                $seller_list = $wpdb->get_results("SELECT * FROM " . HIFI_SELLER_TBL);
                                                  $seller_id=Hifi_action_handler_admin::get_db_post_value(HIFI_PRODUCT_TBL, 'seller_id','product_id', $edit_id);
                                                if ($seller_list) {
                                                    foreach ($seller_list as $single_rec) {
                                                        ?>
                                                        <option value="<?php echo $single_rec->seller_id ?>" <?php
                                                    if($seller_id == $single_rec->seller_id )
                                                    {
                                                      echo "selected";  
                                                    }
                                                    ?>><?php echo $single_rec->seller_name ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="seller_add" style="display: none">
                                            <label class="form_input_title"><?php _e("Seller name", "hifi_domain") ?></label>
                                            <input type="text" required name="seller_name" size="30" placeholder="<?php _e("Seller Name", "hifi_domain") ?>" value="" id="seller_name" spellcheck="true" autocomplete="off">
                                            <label class="form_input_title"><?php _e("Seller Address", "hifi_domain") ?></label>
                                            <input type="text" required name="seller_address" size="30" placeholder="<?php _e("Seller Address", "hifi_domain") ?>" value="" id="seller_address" spellcheck="true" autocomplete="off">
                                            <label class="form_input_title"><?php _e("Seller Mobile no.", "hifi_domain") ?></label>
                                            <input type="number" required name="seller_mobile_no" size="30" placeholder="<?php _e("Seller Mobile Number", "hifi_domain") ?>" value="" id="seller_mobile" spellcheck="true" autocomplete="off">
                                            <label class="form_input_title"><?php _e("Seller ID Proof", "hifi_domain") ?></label>
                                            <input type="text" required name="seller_id_proof" size="30" placeholder="<?php _e("Seller ID Proof", "hifi_domain") ?>" value="" id="seller_id_proof" spellcheck="true" autocomplete="off">
                                            <label class="form_input_title"><?php _e("Photograph", "hifi_domain") ?></label>
                                            <input type="file" name="seller_image" id="seller_photo">
                                        </div>
                                    </div>
                                    <div id="date_wrap" class="product_form">
                                        <label class="form_input_title"><?php _e("Date of Purchase", "hifi_domain") ?></label>
                                        <?php
                                        $dt_purchase=Hifi_action_handler_admin::get_db_post_value(HIFI_PRODUCT_TBL, 'date_of_purchase', 'product_id', $edit_id)?Hifi_action_handler_admin::get_db_post_value(HIFI_PRODUCT_TBL, 'date_of_purchase', 'product_id', $edit_id): date_i18n('Y-m-d H:i:s');
                                        $date_of_purchase= date_i18n(HIFI_DATE_FORMAT, strtotime($dt_purchase));
                                        ?>
                                        <input type="text" required name="date_of_purchase" id="date_of_purchase" size="30" placeholder="<?php _e("Date of Purchase", "hifi_domain") ?>" value="<?php echo $date_of_purchase ?>" id="date_of_purchase" spellcheck="true" autocomplete="off">
                                    </div>
                                    <div id="amount_wrap" class="product_form">
                                        <label class="form_input_title"><?php _e("Purchase amount", "hifi_domain") ?></label>
                                        <input type="number" required name="purchase_amount" size="30" placeholder="<?php _e("Purchase amount", "hifi_domain") ?>" value="<?php echo Hifi_action_handler_admin::get_db_post_value(HIFI_PRODUCT_TBL, 'purchase_amount', 'product_id', $edit_id); ?>" id="purchase_amount" spellcheck="true" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="postbox-container-2" class="postbox-container small-container">
                <div class="meta-box-sortables">
                    <div id="" class="postbox " >
                        <button type="button" class="handlediv button-link" aria-expanded="true"><span class="screen-reader-text">Toggle panel: Publish </span><span class="toggle-indicator" aria-hidden="true"></span></button>
                        <h2 class='hndle'><span><?php _e("Publish", "hifi_domain") ?></span></h2>
                        <div class="inside">
                            <div class="submitbox" id="submitpost">
                                <div id="submit_btn_div">
                                    <?php wp_nonce_field('manage_product'); ?>
                                    <input type="hidden" name="edit_id" value="<?php echo $edit_id ?>">
                                    <?php
                                    if (!isset($_REQUEST['edit_id']) && empty($_REQUEST['edit_id'])) {
                                        ?>
                                        <input type="submit" name="save_product" id="publish" class="button button-primary button-large" value="<?php _e("Add Product", "hifi_domain") ?>">
                                        <?php
                                    }
                                    ?>
<!--                                    <input type="submit" name="save_close_product" id="publish1" class="button button-primary button-large" value="<?php //_e("Save and Close", "hifi_domain") ?>">		-->
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>	
                <?php ?>
            </div>
            
        </form>
        <div class="clear"></div>
    </div>
</div>