<?php
$edit_id = 0;
if (isset($_REQUEST['manage_type']) && $_REQUEST['manage_type'] == 'add') {
    $title = __("Add", "hifi_domain");
} else {
    $title = __("Edit", "hifi_domain");
}

if (isset($_REQUEST['edit_id']) && !empty($_REQUEST['edit_id'])) {
    $edit_id = $_REQUEST['edit_id'];
}
?>
<div class="wrap">
    <h1 class="wp-heading-inline"><?php _e("Manage Buyer", "hifi_domain"); ?></h1>
    <hr class="wp-header-end">
    <div class="metabox-holder hifi_plugin_page category_page">
        <form method="post" id="buyer_form">
            <div id="postbox-container-1" class="postbox-container big-container hifi_buyer_div">
                <div class="meta-box-sortables">
                    <div id="" class="postbox " >
                        <button type="button" class="handlediv button-link" aria-expanded="true"><span class="screen-reader-text">Toggle panel: Getting Started</span><span class="toggle-indicator" aria-hidden="true"></span></button>
                        <h2 class='hndle'><span>  <?php echo $title . ' ' . __("Buyer", "hifi_domain") ?></span></h2>
                        <div class="inside">
                            <div class="main">

                                <div id="titlediv">
                                    <label class="form_input_title"><?php _e("Buyer name", "hifi_domain") ?></label>
                                    <input type="text" required name="buyer_name" size="30" placeholder="<?php _e("Buyer Name", "hifi_domain") ?>" value="<?php echo Hifi_action_handler_admin::get_db_post_value(HIFI_BUYER_TBL, 'buyer_name', 'buyer_id', $edit_id) ?>" id="buyer_name" spellcheck="true" autocomplete="off">
                                    <label class="form_input_title"><?php _e("Buyer Address", "hifi_domain") ?></label>
                                    <input type="text" required name="buyer_address" size="30" placeholder="<?php _e("Buyer Address", "hifi_domain") ?>" value="<?php echo Hifi_action_handler_admin::get_db_post_value(HIFI_BUYER_TBL, 'buyer_address', 'buyer_id', $edit_id) ?>" id="buyer_address" spellcheck="true" autocomplete="off">
                                    <label class="form_input_title"><?php _e("Buyer Mobile no.", "hifi_domain") ?></label>
                                    <input type="number" required name="buyer_mobile_no" size="30" placeholder="<?php _e("Buyer Mobile Number", "hifi_domain") ?>" value="<?php echo Hifi_action_handler_admin::get_db_post_value(HIFI_BUYER_TBL, 'buyer_mobile_no', 'buyer_id', $edit_id) ?>" id="buyer_mobile" spellcheck="true" autocomplete="off">
                                    <label class="form_input_title"><?php _e("Buyer ID Proof", "hifi_domain") ?></label>
                                    <input type="text" required name="buyer_id_proof" size="30" placeholder="<?php _e("Buyer ID Proof", "hifi_domain") ?>" value="<?php echo Hifi_action_handler_admin::get_db_post_value(HIFI_BUYER_TBL, 'buyer_id_proof', 'buyer_id', $edit_id) ?>" id="buyer_id_proof" spellcheck="true" autocomplete="off">
                                     
                                    <label class="form_input_title"><?php _e("Sell Price", "hifi_domain") ?></label>
                                    <input type="number" required name="sell_price" size="30" placeholder="<?php _e("Sell Price", "hifi_domain") ?>" value="<?php echo Hifi_action_handler_admin::get_db_post_value(HIFI_BUYER_TBL, 'sell_price', 'buyer_id', $edit_id) ?>" id="sell_price" spellcheck="true" autocomplete="off">
                                    <label class="form_input_title"><?php _e("Sell Date", "hifi_domain") ?></label>
                                    <input type="text" required name="sell_date" size="30" placeholder="<?php _e("Sell Date", "hifi_domain") ?>" value="<?php echo Hifi_action_handler_admin::get_db_post_value(HIFI_BUYER_TBL, 'sell_date', 'buyer_id', $edit_id) ?>" id="sell_date" spellcheck="true" autocomplete="off">

                                    <label class="form_input_title"><?php _e("Reference person", "hifi_domain") ?></label>
                                    <input type="text" name="buyer_reference_person" size="30" placeholder="<?php _e("Reference person", "hifi_domain") ?>" value="<?php echo Hifi_action_handler_admin::get_db_post_value(HIFI_BUYER_TBL, 'buyer_reference_person', 'buyer_id', $edit_id) ?>" id="buyer_reference_person" spellcheck="true" autocomplete="off">
                                    <label class="form_input_title"><?php _e("Sell Notes", "hifi_domain") ?></label>
                                    <textarea required rows="4" name="buyer_remarks" id="buyer_remarks" placeholder="<?php _e("Sell Notes", "hifi_domain") ?>"><?php echo Hifi_action_handler_admin::get_db_post_value(HIFI_BUYER_TBL, 'buyer_remarks', 'buyer_id', $edit_id) ?></textarea>
                                    
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="postbox-container-2" class="postbox-container small-container">
                <div class="meta-box-sortables">
                    <div id="" class="postbox " >
                        <button type="button" class="handlediv button-link" aria-expanded="true"><span class="screen-reader-text">Toggle panel: Publish </span><span class="toggle-indicator" aria-hidden="true"></span></button>
                        <h2 class='hndle'><span><?php _e("Publish", "hifi_domain") ?></span></h2>
                        <div class="inside">

                            <div class="submitbox" id="submitpost">

                                <div id="back_link_div">

                                    <a href="<?php echo HIFI_ADMIN_URL . 'admin.php?page=hifi_manage_buyer' ?>" class="back_link"><?php _e("Back to list", "hifi_domain") ?></a>
                                    <div class="clear"></div>
                                </div>
                                <br>

                                <div id="submit_btn_div">
                                    <span class="spinner" style="display: none"></span>
                                    <?php wp_nonce_field('manage_buyer_rec'); ?>
                                    <input type="hidden" name="edit_id" value="<?php echo $edit_id ?>">
                                    <?php
                                    if (!isset($_REQUEST['edit_id']) && empty($_REQUEST['edit_id'])) {
                                        ?>
                                        <input type="submit" name="save_buyer" id="publish" class="button button-primary button-large" value="<?php _e("Save", "hifi_domain") ?>">
                                        <?php
                                    }
                                    ?>

                                    <input type="submit" name="save_close_buyer" id="publish1" class="button button-primary button-large" value="<?php _e("Save and Close", "hifi_domain") ?>">		
                                </div>
                                <div class="clear"></div>

                            </div>

                        </div>
                    </div>
                </div>	

                <?php ?>
            </div>
        </form>
        <div class="clear"></div>
    </div>
</div>