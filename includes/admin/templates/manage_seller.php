<?php
$edit_id = 0;
if (isset($_REQUEST['manage_type']) && $_REQUEST['manage_type'] == 'add') {
    $title = __("Add", "hifi_domain");
} else {
    $title = __("Edit", "hifi_domain");
}

if (isset($_REQUEST['edit_id']) && !empty($_REQUEST['edit_id'])) {
    $edit_id = $_REQUEST['edit_id'];
}
?>
<div class="wrap">
    <h1 class="wp-heading-inline"><?php _e("Manage Seller", "hifi_domain"); ?></h1>
    <hr class="wp-header-end">
    <div class="metabox-holder hifi_plugin_page category_page">
        <form method="post" id="seller_form" enctype="multipart/form-data">
            <div id="postbox-container-1" class="postbox-container big-container hifi_seller_div">
                <div class="meta-box-sortables">
                    <div id="" class="postbox " >
                        <button type="button" class="handlediv button-link" aria-expanded="true"><span class="screen-reader-text">Toggle panel: Getting Started</span><span class="toggle-indicator" aria-hidden="true"></span></button>
                        <h2 class='hndle'><span>  <?php echo $title . ' ' . __("Seller", "hifi_domain") ?></span></h2>
                        <div class="inside">
                            <div class="main">

                                <div id="titlediv">
                                    <label class="form_input_title"><?php _e("Seller name", "hifi_domain") ?></label>
                                    <input type="text" required name="seller_name" size="30" placeholder="<?php _e("Seller Name", "hifi_domain") ?>" value="<?php echo Hifi_action_handler_admin::get_db_post_value(HIFI_SELLER_TBL, 'seller_name', 'seller_id', $edit_id) ?>" id="seller_name" spellcheck="true" autocomplete="off">
                                    <label class="form_input_title"><?php _e("Seller Address", "hifi_domain") ?></label>
                                    <input type="text" required name="seller_address" size="30" placeholder="<?php _e("Seller Address", "hifi_domain") ?>" value="<?php echo Hifi_action_handler_admin::get_db_post_value(HIFI_SELLER_TBL, 'seller_address', 'seller_id', $edit_id) ?>" id="seller_address" spellcheck="true" autocomplete="off">
                                    <label class="form_input_title"><?php _e("Seller Mobile no.", "hifi_domain") ?></label>
                                    <input type="number" required name="seller_mobile_no" size="30" placeholder="<?php _e("Seller Mobile Number", "hifi_domain") ?>" value="<?php echo Hifi_action_handler_admin::get_db_post_value(HIFI_SELLER_TBL, 'seller_mobile_no', 'seller_id', $edit_id) ?>" id="seller_mobile" spellcheck="true" autocomplete="off">
                                    <label class="form_input_title"><?php _e("Seller ID Proof", "hifi_domain") ?></label>
                                    <input type="text" required name="seller_id_proof" size="30" placeholder="<?php _e("Seller ID Proof", "hifi_domain") ?>" value="<?php echo Hifi_action_handler_admin::get_db_post_value(HIFI_SELLER_TBL, 'seller_id_proof', 'seller_id', $edit_id) ?>" id="seller_id_proof" spellcheck="true" autocomplete="off">
                                    
                                    <label class="form_input_title"><?php _e("Photograph", "hifi_domain") ?></label>
                                    <?php
                                    $attachment_seller_id=Hifi_action_handler_admin::get_db_post_value(HIFI_SELLER_TBL, 'seller_image', 'seller_id', $edit_id);
                                    $attachment= wp_get_attachment_image_src($attachment_seller_id, 'thumbnail');
                                    if($attachment)
                                    {
                                        echo '<img src="'.$attachment[0].'" height="150" width="150" class="seller_img">';
                                    }
                                    ?>
                                    <input type="file" name="seller_image" id="seller_photo">
                                    
                                    <input type="hidden" name="seller_photo_id" value="<?php echo $attachment_seller_id ?>">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="postbox-container-2" class="postbox-container small-container">
                <div class="meta-box-sortables">
                    <div id="" class="postbox " >
                        <button type="button" class="handlediv button-link" aria-expanded="true"><span class="screen-reader-text">Toggle panel: Publish </span><span class="toggle-indicator" aria-hidden="true"></span></button>
                        <h2 class='hndle'><span><?php _e("Publish", "hifi_domain") ?></span></h2>
                        <div class="inside">

                            <div class="submitbox" id="submitpost">

                                <div id="back_link_div">

                                    <a href="<?php echo HIFI_ADMIN_URL . 'admin.php?page=hifi_manage_seller' ?>" class="back_link"><?php _e("Back to list", "hifi_domain") ?></a>
                                    <div class="clear"></div>
                                </div>
                                <br>

                                <div id="submit_btn_div">
                                    <span class="spinner" style="display: none"></span>
                                    <?php wp_nonce_field('manage_seller'); ?>
                                    <input type="hidden" name="edit_id" value="<?php echo $edit_id ?>">
                                    <?php
                                    if (!isset($_REQUEST['edit_id']) && empty($_REQUEST['edit_id'])) {
                                        ?>
                                        <input type="submit" name="save_seller" id="publish" class="button button-primary button-large" value="<?php _e("Save", "hifi_domain") ?>">
                                        <?php
                                    }
                                    ?>

                                    <input type="submit" name="save_close_seller" id="publish1" class="button button-primary button-large" value="<?php _e("Save and Close", "hifi_domain") ?>">		
                                </div>
                                <div class="clear"></div>

                            </div>

                        </div>
                    </div>
                </div>	

                <?php ?>
            </div>
        </form>
        <div class="clear"></div>
    </div>
</div>