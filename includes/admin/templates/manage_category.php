<?php
$edit_id = 0;
if (isset($_REQUEST['manage_type']) && $_REQUEST['manage_type'] == 'add') {
    $title = __("Add", "hifi_domain");
} else {
    $title = __("Edit", "hifi_domain");
}

if (isset($_REQUEST['edit_id']) && !empty($_REQUEST['edit_id'])) {
    $edit_id = $_REQUEST['edit_id'];
}
?>
<div class="wrap">
    <h1 class="wp-heading-inline"><?php _e("Manage Category", "hifi_domain"); ?></h1>
    <hr class="wp-header-end">
    <div class="metabox-holder hifi_plugin_page category_page">
        <form method="post" id="category_form">
            <div id="postbox-container-1" class="postbox-container big-container hifi_category_div">
                <div class="meta-box-sortables">
                    <div id="" class="postbox " >
                        <button type="button" class="handlediv button-link" aria-expanded="true"><span class="screen-reader-text">Toggle panel: Getting Started</span><span class="toggle-indicator" aria-hidden="true"></span></button>
                        <h2 class='hndle'><span>  <?php echo $title . ' ' . __("Category", "hifi_domain") ?></span></h2>
                        <div class="inside">
                            <div class="main">

                                <div id="titlediv">
                                    <div id="titlewrap">
                                        <input required type="text" name="category_title" size="30" placeholder="<?php _e("Enter title here", "hifi_domain") ?>" value="<?php echo Hifi_action_handler_admin::get_db_post_value(HIFI_CATEGORY_TBL, 'category_name', 'category_id', $edit_id) ?>" id="title" spellcheck="true" autocomplete="off">
                                    </div>

                                    <div id="descwrap">
                                        <textarea required rows="8" name="category_desc" id="category_desc" placeholder="<?php _e("Enter Description here", "hifi_domain") ?>"><?php echo Hifi_action_handler_admin::get_db_post_value(HIFI_CATEGORY_TBL, 'category_desc', 'category_id', $edit_id) ?></textarea>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="postbox-container-2" class="postbox-container small-container">
                <div class="meta-box-sortables">
                    <div id="" class="postbox " >
                        <button type="button" class="handlediv button-link" aria-expanded="true"><span class="screen-reader-text">Toggle panel: Publish </span><span class="toggle-indicator" aria-hidden="true"></span></button>
                        <h2 class='hndle'><span><?php _e("Publish", "hifi_domain") ?></span></h2>
                        <div class="inside">

                            <div class="submitbox" id="submitpost">

                                <div id="back_link_div">

                                    <a href="<?php echo HIFI_ADMIN_URL . 'admin.php?page=hifi_manage_module' ?>" class="back_link"><?php _e("Back to list", "hifi_domain") ?></a>
                                    <div class="clear"></div>
                                </div>
                                <br>
                                <div id="submit_btn_div">
                                    <span class="spinner" style="display: none"></span>
                                    <?php wp_nonce_field('manage_category'); ?>
                                    <input type="hidden" name="edit_id" value="<?php echo $edit_id ?>">
                                    <?php
                                    if (!isset($_REQUEST['edit_id']) && empty($_REQUEST['edit_id'])) {
                                        ?>
                                        <input type="submit" name="save_category" id="publish" class="button button-primary button-large" value="<?php _e("Save", "hifi_domain") ?>">
                                        <?php
                                    }
                                    ?>
                                    <input type="submit" name="save_close_category" id="publish1" class="button button-primary button-large" value="<?php _e("Save and Close", "hifi_domain") ?>">		
                                </div>
                                <div class="clear"></div>

                            </div>

                        </div>
                    </div>
                </div>	
                <?php ?>
            </div>
        </form>
        <div class="clear"></div>
    </div>
</div>