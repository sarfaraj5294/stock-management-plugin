<?php
if (!defined('ABSPATH')) {
    exit;
}
/**
 * Handle Custom Post type
 * @class Hifi_action_handler_admin
 * @since 1.0.0
 * @author Sarfaraj kazi
 */
if (!class_exists('Hifi_action_handler_admin', false)) :

    class Hifi_action_handler_admin {

        /**
         * Constructor
         */
        public function __construct() {
            
        }

        public function hifi_add_new_product() {
            include_once HIFI_PLUGIN_PATH . 'includes/admin/templates/manage_product.php';
        }

        public function hifi_manage_stock() {
            global $wpdb;
            include_once HIFI_PLUGIN_PATH . 'includes/admin/custom_tables/custom_wp_table_product.php';
            $product_obj = new rec_list_product(HIFI_PRODUCT_TBL);
            ?>
            <div class="wrap">
                <h1 class="wp-heading-inline"><?php _e("Products List", "hifi_domain"); ?></h1>
                <hr class="wp-header-end">
                <div id="poststuff">
                    <div id="post-body" class="metabox-holder">
                        <?php
                        $product_obj->views();
                        ?>
                        <?php
                        $move_on_url = '&cat-filter=';
                        ?>
                        <div class="alignleft actions bulkactions hifi_filter_by_category">
                            <?php
                            $cats = $wpdb->get_results('select * from ' . HIFI_CATEGORY_TBL . ' order by category_name asc', ARRAY_A);
                            if ($cats) {
                                ?>
                                <select name="cat-filter" class="hifi-filter-cat">
                                    <option value="" hidden><?php _e("Filter by Category", "hifi_domain") ?></option>
                                    <option value="all"><?php _e("All", "hifi_domain") ?></option>
                                    <?php
                                    foreach ($cats as $cat) {
                                        $selected = '';
                                        if (isset($_GET['cat-filter']) && $_GET['cat-filter'] == $cat['category_id']) {
                                            $selected = ' selected = "selected"';
                                        }
                                        $has_testis = false;
                                        $chk_testis = $wpdb->get_row("select product_id from " . HIFI_PRODUCT_TBL . " where category_id=" . $cat['category_id'], ARRAY_A);
                                        if ($chk_testis['product_id'] > 0) {
                                            ?>
                                            <option value="<?php echo $move_on_url . $cat['category_id']; ?>" <?php echo $selected; ?>><?php echo $cat['category_name']; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <?php
                            }

                            $move_on_company_url = '&company-filter=';
                            ?>

                            <?php
                            $comapnies = $wpdb->get_results('select * from ' . HIFI_COMPANY_TBL . ' order by company_name asc', ARRAY_A);
                            if ($comapnies) {
                                ?>
                                <select name="company-filter" class="hifi-filter-cat">
                                    <option value="" hidden><?php _e("Filter by Company", "hifi_domain") ?></option>
                                    <option value="all"><?php _e("All", "hifi_domain") ?></option>
                                    <?php
                                    foreach ($comapnies as $cat) {
                                        $selected = '';
                                        if (isset($_GET['company-filter']) && $_GET['company-filter'] == $cat['category_id']) {
                                            $selected = ' selected = "selected"';
                                        }
                                        $has_testis = false;
                                        $chk_testis = $wpdb->get_row("select product_id from " . HIFI_PRODUCT_TBL . " where company_id=" . $cat['company_id'], ARRAY_A);
                                        if ($chk_testis['product_id'] > 0) {
                                            ?>
                                            <option value="<?php echo $move_on_company_url . $cat['company_id']; ?>" <?php echo $selected; ?>><?php echo $cat['company_name']; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <?php
                            }
                            ?>

                        </div>
                        <?php ?>
                        <?php
                        ?>
                        <form method="post">
                            <?php
                            $product_obj->prepare_items();
                            $product_obj->search_box('Search', 'search');
                            $product_obj->display();
                            ?>
                        </form>
                        <?php add_thickbox(); ?>
                        <div id="product_buyer_info" style="display:none;">
                            <form name="buyer_info" id="buyer_info" method="post">
                                <label class="form_input_title"><?php _e("Buyer Info", "hifi_domain") ?> </label>
                                <div class="buyer_selctions">
                                    <label class="radio_handler"><input type="radio" value="1" name="select_type" checked><?php _e("Select Buyer", "hifi_domain") ?> </label>

                                    <label class="radio_handler"><input type="radio" value="2" name="select_type"><?php _e("Add New Buyer", "hifi_domain") ?> </label>
                                </div>
                                <div class="buyer_box_div">
                                    <select name="buyer_id"  class="required buyer_box product_form">
                                        <option value="" hidden><?php _e("Select Buyer", "hifi_domain") ?></option>
                                        <?php
                                        $buyer_list = $wpdb->get_results("SELECT * FROM " . HIFI_BUYER_TBL);
                                        if ($buyer_list) {
                                            foreach ($buyer_list as $single_rec) {
                                                ?>
                                                <option value="<?php echo $single_rec->buyer_id ?>"><?php echo $single_rec->buyer_name ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="buyer_add" style="display:none;">
                                    <label class="form_input_title"><?php _e("Buyer name", "hifi_domain") ?></label>
                                    <input type="text" required name="buyer_name" size="30" placeholder="<?php _e("Buyer Name", "hifi_domain") ?>" value="" id="buyer_name" spellcheck="true" autocomplete="off">
                                    <label class="form_input_title"><?php _e("Buyer Address", "hifi_domain") ?></label>
                                    <input type="text" required name="buyer_address" size="30" placeholder="<?php _e("Buyer Address", "hifi_domain") ?>" value="" id="buyer_address" spellcheck="true" autocomplete="off">
                                    <label class="form_input_title"><?php _e("Buyer Mobile no.", "hifi_domain") ?></label>
                                    <input type="number" required name="buyer_mobile_no" size="30" placeholder="<?php _e("Buyer Mobile Number", "hifi_domain") ?>" value="" id="buyer_mobile" spellcheck="true" autocomplete="off">
                                    <label class="form_input_title"><?php _e("Buyer ID Proof", "hifi_domain") ?></label>
                                    <input type="text" required name="buyer_id_proof" size="30" placeholder="<?php _e("Buyer ID Proof", "hifi_domain") ?>" value="" id="seller_id_proof" spellcheck="true" autocomplete="off">

                                </div>
                                <div class="buyer_extra_fields">
                                    <label class="form_input_title"><?php _e("Sell Price", "hifi_domain") ?></label>
                                    <input type="number" required name="sell_price" size="30" placeholder="<?php _e("Sell Price", "hifi_domain") ?>" value="" id="sell_price" spellcheck="true" autocomplete="off">
                                    <label class="form_input_title"><?php _e("Sell Date", "hifi_domain") ?></label>
                                    <input type="text" required name="sell_date" size="30" placeholder="<?php _e("Sell Date", "hifi_domain") ?>" value="" id="sell_date" spellcheck="true" autocomplete="off">

                                    <label class="form_input_title"><?php _e("Reference person", "hifi_domain") ?></label>
                                    <input type="text" name="buyer_reference_person" size="30" placeholder="<?php _e("Reference person", "hifi_domain") ?>" value="" id="buyer_reference_person" spellcheck="true" autocomplete="off">

                                    <label class="form_input_title"><?php _e("Sell Notes", "hifi_domain") ?></label>
                                    <textarea required rows="3" name="buyer_remarks" id="buyer_remarks" placeholder="<?php _e("Sell Notes", "hifi_domain") ?>"></textarea>
                                </div>
                                <div class="buyer-submit-btn">
                                    <?php wp_nonce_field('buyer_info') ?>

                                    <input type="hidden" id="buyer_product_id" value="" name="buyer_product_id">
                                    <input type="submit" class="button button-secondary" value="<?php _e("Submit", "hifi_domain"); ?>" name="buyer_btn"  id="buyer_btn">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }

        public function hifi_inventory_status() {
            
        }

        public function hifi_report() {
            global $wpdb;
            ?>
            <div class="wrap hifi_reports">
                <h1 class="wp-heading-inline"><?php _e("Reports", "hifi_domain"); ?></h1>
                <hr class="wp-header-end">
                <div id="poststuff">
                    <div id="post-body" class="metabox-holder" >
                        <form method="post"id="report_form">
                            <div class="alignleft actions bulkactions hifi_reports_div">

                                <select name="report_type" id="report_type" class="">
                                    <option value="" ><?php _e("Select report type", 'hifi_plugin') ?></option>
                                    <option value="1" <?php
                                    if(isset($_REQUEST['report_type']) && $_REQUEST['report_type']==1)
                                    {
                                        echo "selected";
                                    }
                                        ?>><?php _e("Purchase Report", 'hifi_plugin') ?></option>
                                    <option value="0" <?php
                                    if(isset($_REQUEST['report_type']) && $_REQUEST['report_type']==0)
                                    {
                                        echo "selected";
                                    }
                                        ?> ><?php _e("Sales Report", 'hifi_plugin') ?></option>
                                </select>
                                <select name="report_category" class="" id="report_category">
                                    <option value="" ><?php _e("Select Category", "hifi_domain") ?></option>
                                    <?php
                                    $cats = $wpdb->get_results('select * from ' . HIFI_CATEGORY_TBL . ' order by category_name asc', ARRAY_A);
                                    if ($cats) {
                                        foreach ($cats as $cat) {
                                            $selected = '';
                                            if (isset($_REQUEST['report_category']) && $_REQUEST['report_category'] == $cat['category_id']) {
                                                $selected = ' selected = "selected"';
                                            }
                                            $has_testis = false;
                                            $chk_testis = $wpdb->get_row("select product_id from " . HIFI_PRODUCT_TBL . " where category_id=" . $cat['category_id'], ARRAY_A);
                                            if ($chk_testis['product_id'] > 0) {
                                                ?>
                                                <option value="<?php echo $cat['category_id']; ?>" <?php echo $selected; ?>><?php echo $cat['category_name']; ?></option>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </select>
                                <label id="reportrange" class="reportrange" style="">
                                    <i class="fa fa-calendar"></i>&nbsp;
                                    <?php
                                    $report_full_date=isset($_REQUEST['report_full_date'])?$_REQUEST['report_full_date']:"";
                                    ?>
                                    <span><?php echo $report_full_date; ?></span> <i class="fa fa-caret-down"></i>
                                    <input type="hidden" id="report_start_date" name="report_start_date" >
                                    <input type="hidden" id="report_end_date" name="report_end_date" >
                                    <input type="hidden" id="report_full_date" name="report_full_date" >
                                </label>
                                <?php wp_nonce_field('get_report') ?>
                                <input type="submit" class="button button-secondary report-btn" value="<?php _e("Get Report", "hifi_domain") ?>">

                            </div>
                            <label class="hifi-validation-error" id="report_error" style="display:none"></label>
                        </form>
                    </div>
                    <?php
                    global $wpdb;
                    $wp_nonne = isset($_POST['_wpnonce']) ? $_POST['_wpnonce'] : "";
                    if (wp_verify_nonce($wp_nonne, 'get_report')) {
                        ?>
                    
                    <div class="table-of-contents" style="display: block">
                            <?php
                            $sql = "SELECT * FROM `" . HIFI_PRODUCT_TBL . '`';
                            $sql .= ' JOIN ' . HIFI_CATEGORY_TBL . ' ON ' . HIFI_PRODUCT_TBL . '.category_id = ' . HIFI_CATEGORY_TBL . '.category_id';
                            $sql .= ' JOIN ' . HIFI_COMPANY_TBL . ' ON ' . HIFI_PRODUCT_TBL . '.company_id = ' . HIFI_COMPANY_TBL . '.company_id ';
                            $sql .= ' JOIN ' . HIFI_SELLER_TBL . ' ON ' . HIFI_PRODUCT_TBL . '.seller_id = ' . HIFI_SELLER_TBL . '.seller_id ';
                            if(isset($_POST['product_status']) && $_POST['product_status']==0)
                            {
                                $sql .= ' JOIN ' . HIFI_BUYER_TBL . ' ON ' . HIFI_PRODUCT_TBL . '.buyer_id = ' . HIFI_BUYER_TBL . '.buyer_id ';
                            }
                            
                            $sql .= ' WHERE (' . HIFI_PRODUCT_TBL . '.product_created_date BETWEEN "'.$_POST['report_start_date'].'" AND "'.$_POST['report_end_date'].'")';
                            $sql .= ' AND ' . HIFI_PRODUCT_TBL . '.product_status=' . $_POST['report_type'];
                            $sql .= ' AND ' . HIFI_PRODUCT_TBL . '.category_id=' . $_POST['report_category'];
                           
                            $product_res = $wpdb->get_results($sql);
                            ?>
                            <table class="wp-list-table widefat fixed striped hifi-report">
                                <thead>
                                <tr>
                                    <th><?php _e("No", "hifi_domain") ?></th>
                                    <th><?php _e("Name", "hifi_domain") ?></th>
                                    <th><?php _e("Category Name", "hifi_domain") ?></th>
                                    <th><?php _e("Company Name", "hifi_domain") ?></th>
                                    <th><?php _e("Seller Name", "hifi_domain") ?></th>
                                    <?php
                                    if(isset($_POST['product_status']) && $_POST['product_status']==0)
                                    {
                                        ?>
                                    <th><?php _e("Buyer Name", "hifi_domain") ?></th>
                                    <?php
                                    }
                                    ?>
                                    
                                    <th><?php _e("Product IMEI", "hifi_domain") ?></th>
                                    <th><?php _e("Price", "hifi_domain") ?></th>
                                    <th><?php _e("Purchase On", "hifi_domain") ?></th>
                                    <th><?php _e("Modify On", "hifi_domain") ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if($product_res)
                                    {
                                        $i=1;
                                        foreach ($product_res as $single_product)
                                        {
                                            
                                            ?>
                                            <tr>
                                                <td>
                                                    <?php echo $i++ ?>
                                                </td>
                                                <td>
                                                    <?php echo $single_product->product_title ?>
                                                </td>
                                                <td>
                                                    <?php echo $single_product->category_name ?>
                                                </td>
                                                <td>
                                                    <?php echo $single_product->company_name ?>
                                                </td>
                                                <td>
                                                    <?php echo $single_product->seller_name ?>
                                                </td>
                                            <?php
                                            if(isset($_POST['product_status']) && $_POST['product_status']==0)
                                            {
                                                ?>
                                                <td>
                                                    <?php echo $single_product->buyer_name ?>
                                                </td>
                                                <?php
                                            }
                                            ?>
                                                <td>
                                                    <?php echo $single_product->product_IMEI ?>
                                                </td>
                                                <td>
                                                    <?php echo $single_product->purchase_amount ?>
                                                </td>
                                                <td>
                                                    <?php echo $single_product->product_created_date ?>
                                                </td>
                                                <td>
                                                    <?php echo $single_product->product_updated_date ?>
                                                </td>
                                            
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <?php
                    }
                    ?>

                </div>
            </div>
            <?php
        }

        public function hifi_manage_module() {
            if (isset($_REQUEST['manage_rec']) && $_REQUEST['manage_rec'] == '1') {
                include_once HIFI_PLUGIN_PATH . 'includes/admin/templates/manage_category.php';
            } else {
                include_once HIFI_PLUGIN_PATH . 'includes/admin/custom_tables/custom_wp_table_category.php';
                $obj = new rec_list_category(HIFI_CATEGORY_TBL);
                ?>
                <div class="wrap">
                    <h1 class="wp-heading-inline"><?php _e("Categories List", "hifi_domain"); ?> <a href="<?php echo HIFI_ADMIN_URL . 'admin.php?page=hifi_manage_module&manage_rec=1&manage_type=add' ?>" class="page-title-action aria-button-if-js" role="button" aria-expanded="false"><?php _e("Add New", "hifi_domain") ?></a></h1>
                    <hr class="wp-header-end">

                    <div id="poststuff">
                        <div id="post-body" class="metabox-holder">
                            <form method="post">
                                <?php
                                $obj->prepare_items();
                                $obj->display();
                                ?>
                            </form>
                        </div>
                    </div>
                </div>
                <?php
            }
        }

        public function hifi_manage_company() {
            if (isset($_REQUEST['manage_rec']) && $_REQUEST['manage_rec'] == '1') {
                include_once HIFI_PLUGIN_PATH . 'includes/admin/templates/manage_company.php';
            } else {
                include_once HIFI_PLUGIN_PATH . 'includes/admin/custom_tables/custom_wp_table_company.php';
                $company_obj = new rec_list_company(HIFI_COMPANY_TBL);
                ?>
                <div class="wrap">
                    <h1 class="wp-heading-inline"><?php _e("Companies List", "hifi_domain"); ?><a href="<?php echo HIFI_ADMIN_URL . 'admin.php?page=hifi_manage_company&manage_rec=1&manage_type=add' ?>" class="page-title-action aria-button-if-js" role="button" aria-expanded="false"><?php _e("Add New", "hifi_domain") ?></a></h1>
                    <hr class="wp-header-end">
                    <div id="poststuff">
                        <div id="post-body" class="metabox-holder">
                            <form method="post">
                                <?php
                                $company_obj->prepare_items();
                                $company_obj->display();
                                ?>
                            </form>
                        </div>
                    </div>
                </div>
                <?php
            }
        }

        public function hifi_manage_buyer() {
            if (isset($_REQUEST['manage_rec']) && $_REQUEST['manage_rec'] == '1') {
                include_once HIFI_PLUGIN_PATH . 'includes/admin/templates/manage_buyer.php';
            } else {
                include_once HIFI_PLUGIN_PATH . 'includes/admin/custom_tables/custom_wp_table_buyer.php';
                $buyer_obj = new rec_list_buyer(HIFI_BUYER_TBL);
                ?>
                <div class="wrap">
                    <h1 class="wp-heading-inline"><?php _e("Buyers List", "hifi_domain"); ?></h1>
                    <hr class="wp-header-end">
                    <div id="poststuff">
                        <div id="post-body" class="metabox-holder">
                            <form method="post">
                                <?php
                                $buyer_obj->prepare_items();
                                $buyer_obj->display();
                                ?>
                            </form>
                        </div>
                    </div>
                </div>
                <?php
            }
        }

        public function hifi_manage_seller() {
            if (isset($_REQUEST['manage_rec']) && $_REQUEST['manage_rec'] == '1') {
                include_once HIFI_PLUGIN_PATH . 'includes/admin/templates/manage_seller.php';
            } else {
                include_once HIFI_PLUGIN_PATH . 'includes/admin/custom_tables/custom_wp_table_seller.php';
                $seller_obj = new rec_list_seller(HIFI_SELLER_TBL);
                ?>
                <div class="wrap">
                    <h1 class="wp-heading-inline"><?php _e("Sellers List", "hifi_domain"); ?><a href="<?php echo HIFI_ADMIN_URL . 'admin.php?page=hifi_manage_seller&manage_rec=1&manage_type=add' ?>" class="page-title-action aria-button-if-js" role="button" aria-expanded="false"><?php _e("Add New", "hifi_domain") ?></a></h1>
                    <hr class="wp-header-end">
                    <div id="poststuff">
                        <div id="post-body" class="metabox-holder">
                            <form method="post">
                                <?php
                                $seller_obj->prepare_items();
                                $seller_obj->display();
                                ?>
                            </form>
                        </div>
                    </div>
                </div>
                <?php
            }
        }

        public static function get_db_post_value($table_name, $field_name = '', $edit_field = '', $edit_id = 0) {
            global $wpdb;
            if ($edit_field == '') {
                $edit_field = $field_name;
            }
            if ($edit_id) {
                $fetch_row = $wpdb->get_row("SELECT * FROM " . $table_name . " WHERE `" . $edit_field . "` = " . $edit_id, ARRAY_A);
//                echo $wpdb->last_query;
                $field_val = isset($fetch_row[$field_name]) ? $fetch_row[$field_name] : "";
            } else {
                $field_val = isset($_REQUEST[$field_name]) ? $_REQUEST[$field_name] : "";
            }
            return $field_val;
        }

    }

    endif;
return new Hifi_action_handler_admin();
