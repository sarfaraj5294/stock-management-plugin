<?php

if (!defined('ABSPATH')) {
    exit;
}
/**
 * Handle Front Scripts and StyleSheets
 * @class hifi_front_assets
 * @since 1.0.0
 */
if (!class_exists('Hifi_frontend_assets', false)) :

    class Hifi_frontend_assets {

        /**
         * Hook in tabs.
         */
        public function __construct() {
            add_action('wp_enqueue_scripts', array($this, 'hifi_frontend_styles'));
            add_action('wp_enqueue_scripts', array($this, 'hifi_frontend_scripts'));
        }

        /**
         * Enqueue styles.
         */
        public function hifi_frontend_styles() {

            $cssVersion = filemtime(HIFI_PLUGIN_PATH . 'assets/css/hifi_front_custom_css.css');
            wp_enqueue_style('hifi_custom-style', HIFI_PLUGIN_URL . 'assets/css/hifi_front_custom_css.css', array(), $cssVersion);

            wp_enqueue_style('font-awsome_css', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css');
        }

        /**
         * Enqueue scripts.
         */
        public function hifi_frontend_scripts() {
            //wp_enqueue_media();
            $jsVersion = filemtime(HIFI_PLUGIN_PATH . 'assets/js/hifi_front_custom.js');
            wp_register_script('hifi_custom-js-file', HIFI_PLUGIN_URL . 'assets/js/hifi_front_custom.js', array('jquery'), $jsVersion, true);
            
            wp_enqueue_script("hifi_custom-js-file");
            wp_localize_script('hifi_custom-js-file', 'frontend_veriables', array('ajax_url' => HIFI_ADMIN_AJAX_URL, 'home_url' => HIFI_HOME_URL, 'site_url' => HIFI_SITE_URL,'required_field'=>__("Required field","hifi_domain"),'required_field_chk'=>__("Select atleast one option","hifi_domain"),'required_field_chk_one'=>__("Please select option","hifi_domain"),'email_err_msg'=>__("Enter Valid Email","hifi_domain"),'policy_submit'=>__("Privacy answers saved. Please wait","hifi_domain"),'consent_submit'=>__("Consent data submited. Please wait","hifi_domain"),'name_err_msg'=>__("Enter name","hifi_domain")));
        }

    }

    endif;
return new hifi_frontend_assets();
