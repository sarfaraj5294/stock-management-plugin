<?php

if (!defined('ABSPATH')) {
    exit;
}

/**
 * Handle all Front Requests
 *
 * @class Hifi_frontend
 * @since 1.0.0
 * @author Sarfaraj kazi
 */
if (!class_exists('Hifi_frontend', false)) :
class Hifi_frontend {

    /**
     * Constructor.
     */
    public function __construct() {
        include_once HIFI_PLUGIN_PATH . 'includes/frontend/hfi_action_handler.php';
        include_once HIFI_PLUGIN_PATH . 'includes/frontend/hifi_assets.php';   
    }
   
}
endif;
return new Hifi_frontend();
