<?php

/**
 * HIFI setup
 *
 * @package HIFI
 * @since    1.0.0
 */
defined('ABSPATH') || exit;

/**
 * Main HIFI Lite Class.
 *
 * @class HIFI
 * @author Sarfaraj kazi
 */
final class HIFI {

    /**
     * HIFI Lite version.
     *
     * @var string
     */
    public $version = '1.0.0';

    /**
     * The single instance of the class.
     *
     * @since 2.1
     */
    protected static $_instance = null;

    /**
     * Session instance.
     *
     * @var HIFI_Session|HIFI_Session_Handler
     */
    public $session = null;

    /**
     * Query instance.
     *
     * @var HIFI_Query
     */
    public $query = null;

    /**
     * Main HIFI Instance.
     *
     * Ensures only one instance of HIFI is loaded or can be loaded.
     *
     * @since 1.0.0
     * @static
     * @see hifi_getInstance()
     * @return HIFI - Main instance.
     */
    public static function hifi_instance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * hifi Constructor.
     */
    public function __construct() {
        $this->hifi_define_constants();
        $this->hifi_includes();

        $this->hifi_init_hooks();
    }

    /**
     * Define Constants
     */
    function hifi_define_constants() {
        global $wpdb;
        $upload_dir = wp_upload_dir();
        $random_number = random_int(111111, 999999999);

        /*
         * plugin constants
         */

        $this->hifi_define("HIFI_PLUGIN_URL", trailingslashit(plugin_dir_url(__DIR__)));
        $this->hifi_define("HIFI_PLUGIN_PATH", trailingslashit(plugin_dir_path(__DIR__)));
        $this->hifi_define('HIFI_VERSION', $this->version);
        $this->hifi_define('HIFI_DATE_FORMAT', get_option('date_format', true));
        $this->hifi_define('HIFI_TIME_FORMAT', get_option('time_format', true));
        $this->hifi_define('HIFI_DB_DATE_FORMAT', "Y-m-d H:i:s");
        /*
         * urls and site info
         */
        $this->hifi_define("HIFI_ADMIN_URL", admin_url());
        
        $this->hifi_define("HIFI_ADMIN_AJAX_URL", admin_url('admin-ajax.php'));
        $this->hifi_define("HIFI_HOME_URL", trailingslashit(home_url()));
        $this->hifi_define("HIFI_SITE_URL", trailingslashit(site_url()));

        $this->hifi_define("HIFI_SITE_NAME", get_bloginfo('name'));
        $this->hifi_define("HIFI_BLOG_NAME", wp_specialchars_decode(get_option('blogname'), ENT_QUOTES));
        $this->hifi_define("HIFI_SITE_DESC", get_bloginfo('description'));
        $this->hifi_define("HIFI_ADMIN_EMAIL", get_bloginfo('admin_email'));
        $this->hifi_define('HIFI_ENABLE', 1);
        $this->hifi_define('HIFI_DISABLE', 0);
        $this->hifi_define('HIFI_SOLD', 0);
        $this->hifi_define('HIFI_IN_STOCK', 1);
        $this->hifi_define('HIFI_IN_REPAIRING', 2);
        $this->hifi_define('HIFI_RANDOM_NUMBER', $random_number);
        $this->hifi_define('HIFI_CATEGORY_TBL', $wpdb->prefix . 'hifi_category_tbl');
        $this->hifi_define('HIFI_COMPANY_TBL', $wpdb->prefix . 'hifi_company_tbl');
        $this->hifi_define('HIFI_SELLER_TBL', $wpdb->prefix . 'hifi_seller_tbl');
        $this->hifi_define('HIFI_BUYER_TBL', $wpdb->prefix . 'hifi_buyer_tbl');
        $this->hifi_define('HIFI_PRODUCT_TBL', $wpdb->prefix . 'hifi_product_tbl');
    }

    /**
     * Define constant if not already set.
     * 
     * @param string      $name  Constant name.
     * @param string|bool $value Constant value.
     */
    private function hifi_define($name, $value) {
        if (!defined($name)) {
            define($name, $value);
        }
    }

    /**
     * What type of request is this?
     *
     * @param  string $type admin or frontend.
     * @return bool
     */
    private function hifi_is_request($type) {
        switch ($type) {
            case 'admin':
                return (is_admin() || defined('DOING_AJAX'));
            case 'frontend':
                return (!is_admin() || defined('DOING_AJAX') );
        }
    }

    /**
     * Include required core files used in admin and on the frontend.
     */
    function hifi_includes() {
        if ($this->hifi_is_request('admin')) {

            include_once HIFI_PLUGIN_PATH . 'includes/admin/hifi_admin.php';
        }
        if ($this->hifi_is_request('frontend')) {
            include_once HIFI_PLUGIN_PATH . 'includes/frontend/hifi_frontend.php';
        }
    }

    function hifi_init_hooks() {
        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();
        if ($wpdb->get_var("show tables like '" . HIFI_CATEGORY_TBL . "'") != HIFI_CATEGORY_TBL) {
            $sql_lang = "CREATE TABLE `" . HIFI_CATEGORY_TBL . "` (
                         `category_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                         `category_name` text NULL,
                         `category_slug` text NULL,
                         `category_desc` text NULL,
                         `category_created_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                         `category_updated_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                         PRIMARY KEY (`category_id`)
                       ) ENGINE=InnoDB $charset_collate;";
            $wpdb->query($sql_lang);
        }
        if ($wpdb->get_var("show tables like '" . HIFI_COMPANY_TBL . "'") != HIFI_COMPANY_TBL) {
            $sql_company_lang = "CREATE TABLE `" . HIFI_COMPANY_TBL . "` (
                         `company_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                         `company_name` text NULL,
                         `company_slug` text NULL,
                         `company_desc` text NULL,
                         `company_created_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                         `company_updated_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                         PRIMARY KEY (`company_id`)
                       ) ENGINE=InnoDB $charset_collate;";
            $wpdb->query($sql_company_lang);
        }
        if ($wpdb->get_var("show tables like '" . HIFI_SELLER_TBL . "'") != HIFI_SELLER_TBL) {
            $sql_seller_lang = "CREATE TABLE `" . HIFI_SELLER_TBL . "` (
                         `seller_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                         `seller_name` text NULL,
                         `seller_address` text NULL,
                         `seller_mobile_no` varchar(12) NULL,
                         `seller_id_proof` text NULL,
                         `seller_image` bigint(20) NULL,
                         `seller_created_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                         `seller_updated_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                         PRIMARY KEY (`seller_id`)
                       ) ENGINE=InnoDB $charset_collate;";
            $wpdb->query($sql_seller_lang);
        }
        if ($wpdb->get_var("show tables like '" . HIFI_BUYER_TBL . "'") != HIFI_BUYER_TBL) {
            $sql_buyer_lang = "CREATE TABLE `" . HIFI_BUYER_TBL . "` (
                         `buyer_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                         `product_id` bigint(20) unsigned NULL,
                         `buyer_name` text  NULL,
                         `buyer_address` text NULL,
                         `buyer_mobile_no` varchar(12) NULL,
                         `buyer_id_proof` text NULL,
                         `buyer_image` text NULL,
                         `sell_price` decimal NULL,
                         `sell_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                         `buyer_reference_person` text NULL,
                         `buyer_remarks` text NULL,
                         `buyer_created_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                         `buyer_updated_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                         PRIMARY KEY (`buyer_id`)
                       ) ENGINE=InnoDB $charset_collate;";
            $wpdb->query($sql_buyer_lang);
        }
        if ($wpdb->get_var("show tables like '" . HIFI_PRODUCT_TBL . "'") != HIFI_PRODUCT_TBL) {
            $sql_product_lang = "CREATE TABLE `" . HIFI_PRODUCT_TBL . "` (
                         `product_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                         `category_id` bigint(20) unsigned  NULL,
                         `company_id` bigint(20) unsigned NULL,
                         `seller_id` bigint(20) unsigned NULL,
                         `buyer_id` bigint(20) unsigned  NULL,
                         `product_IMEI` text NULL,
                         `product_model_number` text NULL,
                         `product_title` text NULL,
                         `product_description` text NULL,
                         `product_other_info` LONGTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                         `date_of_purchase` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                         `purchase_amount` decimal NULL,
                         `product_status` tinyint(4) DEFAULT '1',
                         `date_of_sell` TIMESTAMP,
                         `product_created_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                         `product_updated_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                         PRIMARY KEY (`product_id`)
                       ) ENGINE=InnoDB $charset_collate;";
            $wpdb->query($sql_product_lang);
        }
    }

}
